:mod:`blur` -- Blurring functions
=================================

.. automodule:: src.blur
   :members:
   :undoc-members:
   :special-members: