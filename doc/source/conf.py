# Configuration file for the Sphinx documentation builder.
#

import os
import sys

sys.path.insert(0, os.path.abspath("../../"))
sys.path.insert(0, os.path.abspath("../../src"))

# -- Project information -----------------------------------------------------

project = "SN and Dark matter density"
copyright = "2023"
author = "Antoine"

extensions = ["sphinx.ext.autosummary",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
]
templates_path = ["templates/"]

html_static_path = ["_static"]
exclude_patterns = [".build/*", "templates/*", "ext/*.rst"]

html_theme = "sphinx_rtd_theme"
napoleon_google_docstring = False
autodoc_typehints = "none"
