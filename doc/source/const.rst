:mod:`const` -- Constants
=========================

.. automodule:: src.const
   :members:
   :special-members:
   :undoc-members: