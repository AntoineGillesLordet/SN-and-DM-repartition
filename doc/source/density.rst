:mod:`density` -- Density computing
===================================

.. automodule:: src.density
   :members:
   :undoc-members:
   :special-members: