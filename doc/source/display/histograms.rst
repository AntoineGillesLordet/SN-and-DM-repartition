:mod:`histograms` -- Histograms plots
=====================================

.. automodule:: src.display.histograms
   :members:
   :undoc-members:
   :special-members: