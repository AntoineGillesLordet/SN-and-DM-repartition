:mod:`display` -- Display related functions
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   histograms
   mollweide
   multi_display
   view_3d