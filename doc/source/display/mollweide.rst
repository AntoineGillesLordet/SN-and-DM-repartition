:mod:`mollweide` -- Mollweide projection plots
==============================================

.. automodule:: src.display.mollweide
   :members:
   :undoc-members:
   :special-members:
