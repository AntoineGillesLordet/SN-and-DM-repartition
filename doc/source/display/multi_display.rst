:mod:`multi_display` -- Display of multiple plots
=================================================

.. automodule:: src.display.multi_display
   :members:
   :undoc-members:
   :special-members:
