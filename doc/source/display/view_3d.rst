:mod:`view_3d` -- 3D plots
==========================

.. automodule:: src.display.view_3d
   :members:
   :undoc-members:
   :special-members:
