:mod:`evaluation` -- Posterior quality quantification
=====================================================

.. automodule:: src.evaluation
   :members:
   :undoc-members:
   :special-members: