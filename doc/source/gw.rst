:mod:`gw` -- ``BILBY`` Simulation template
==========================================

.. automodule:: src.gw
   :members:
   :undoc-members:
   :special-members: