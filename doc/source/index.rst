SN and Dark matter density
==========================

This is a project aiming to correlate the positions of SuperNovae with the repartition of Dark matter in the universe.

Here is its documentation :

.. toctree::
   :maxdepth: 2
   :caption: Code structure

   load
   display/index
   shell_extract
   density
   volume_search
   gw
   evaluation
   blur
   const
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
