:mod:`load` -- Loading functions
================================


:mod:`loading` -- Data reading and loading
------------------------------------------

.. automodule:: src.load.loading
   :members:
   :undoc-members:
   :special-members:

:mod:`conversion` -- Data type conversion
-----------------------------------------

.. automodule:: src.load.conversion
   :members:
   :undoc-members:
   :special-members:

   
:mod:`misc` -- Miscellaneous
----------------------------

.. automodule:: src.load.misc
   :members:
   :undoc-members:
   :special-members: