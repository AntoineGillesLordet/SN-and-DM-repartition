:mod:`shell_extract` -- Functions for data extraction in shells
===============================================================

.. automodule:: src.shell_extract
   :members:
   :undoc-members:
   :special-members: