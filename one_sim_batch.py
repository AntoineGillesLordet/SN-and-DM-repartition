import argparse

parser = argparse.ArgumentParser(description="Run bilby for some event")

parser.add_argument("-line", "--complete_line", dest="line", type=str)

parser.add_argument("-i", "--input", dest="in", type=str)
parser.add_argument("-r", "--running", dest="run", type=str)
parser.add_argument("-o", "--output", dest="out", type=str)
parser.add_argument(
    "-icfg",
    "--interferometers-configuration",
    nargs="+",
    dest="if_list",
    type=str,
    default=["H1", "L1"],
)
parser.add_argument("--event-type", dest="event_type", type=str, default="SN")
parser.add_argument("-npool", dest="npool", type=int, default=1)

args = vars(parser.parse_args())

import numpy as np
import csv
import time
from src import BNS
from astropy.table import QTable
import sys
import fcntl

line = list(csv.reader([args["line"]], delimiter=" "))[0]

if args["event_type"] == "SN":
    label = line[1]
    ra = float(line[2])
    dec = float(line[3])
    ld = float(line[15])
elif args["event_type"] == "DM":
    label = line[-1]
    ra = float(line[3])
    dec = float(line[4])
    ld = float(line[6])

print(f"Run labelled {label}. Inputed parameters : RA={ra}, DEC={dec}, l_d={ld}")

with open(args["in"], "r+") as csv_in:
    fcntl.lockf(csv_in, fcntl.LOCK_EX)
    table = QTable.read(args["in"])
    if args["event_type"] == "SN":
        table = table[table["IAUID"] != label]
    elif args["event_type"] == "DM":
        table = table[table["label"] != label]
    table.write(args["in"], overwrite=True)
    fcntl.lockf(csv_in, fcntl.F_UNLCK)

with open(args["run"], "a") as csv_run:
    fcntl.lockf(csv_run, fcntl.LOCK_EX)
    csv_run.write(args["line"] + "\n")
    fcntl.lockf(csv_run, fcntl.F_UNLCK)


ra = ra * np.pi / 180
dec = dec * np.pi / 180

label, opt_SNR = BNS(
    label=label,
    ra=ra,
    dec=dec,
    luminosity_distance=ld,
    npool=args["npool"],
    interferometer_list=args["if_list"],
    fixed=["chi_1", "chi_2", "lambda_1", "lambda_2"],
    relative_binning=True,
)

with open(args["run"], "r+") as csv_run:
    fcntl.lockf(csv_run, fcntl.LOCK_EX)
    table = QTable.read(args["run"])
    if args["event_type"] == "SN":
        table = table[table["IAUID"] != label]
    elif args["event_type"] == "DM":
        table = table[table["label"] != label]
    table.write(args["run"], overwrite=True)
    fcntl.lockf(csv_run, fcntl.F_UNLCK)


with open(args["out"], "a") as csv_out:
    fcntl.lockf(csv_out, fcntl.LOCK_EX)
    if args["event_type"] == "SN":
        csv_out.write(args["line"] + ' ' + str(opt_SNR) + '\n')
    elif args["event_type"] == "DM":
        csv_out.write(args["line"] + f" {label is not None}\n")
    fcntl.lockf(csv_out, fcntl.F_UNLCK)