#!/bin/bash
#SBATCH -p cops
#SBATCH --job-name=lss-sn
#SBATCH --nodes=1
#SBATCH --tasks-per-node=64
#SBATCH --cpus-per-task=1
#SBATCH --time=7-00:00:00

echo "Starting..."

source ~/conda-env/setup-environment.sh -i

IFS=" "

function cancel {
  echo Received signal : SIGINT 
  echo "Cleaning running files..."
while read -r ztf iauid others
do
    echo "$ztf $iauid $others" >> $1
    sed -i '/'"${iauid}"'/d' $2
    rm -r -f outdir/$iauid
done < <(tail -n +69 $2)
exit 1
}

trap "cancel $1 $2" SIGINT

while read -r line
do
    echo "Simulation submitted"
    IFS='"'; arrline=($line);
    srun --ntasks 1 --cpus-per-task 8 --exclusive -J "$arrline[-1]" python one_sim_batch.py -line "$line" -i $1 -r $2 -o $3 -icfg H1 L1 --event-type SN -npool 8 &
    IFS=" ";
done < <(tail -n +70 $1)

wait
