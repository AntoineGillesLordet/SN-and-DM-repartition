#!/bin/bash
#SBATCH -p fermi
#SBATCH --nodes=1

source ~/conda-env/setup-environment.sh -i

python test_gw.py

wait