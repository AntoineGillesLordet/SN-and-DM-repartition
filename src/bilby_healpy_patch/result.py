import inspect
import os
from copy import copy
import multiprocessing
from functools import partial
import numpy as np
from typing import Any

from bilby.core.utils import logger


def __eval_l(likelihood, params) -> Any:
    likelihood.parameters.update(params)
    return likelihood.log_likelihood()


def get_weights_for_reweighting(
    result,
    new_likelihood=None,
    new_prior=None,
    old_likelihood=None,
    old_prior=None,
    resume_file=None,
    n_checkpoint=5000,
    npool=1,
) -> tuple:
    """Calculate the weights for reweight()

    See bilby.core.result.reweight() for help with the inputs

    Returns
    =======
    ln_weights: array
        An array of the natural-log weights
    new_log_likelihood_array: array
        An array of the natural-log likelihoods from the new likelihood
    new_log_prior_array: array
        An array of the natural-log priors
    old_log_likelihood_array: array
        An array of the natural-log likelihoods from the old likelihood
    old_log_prior_array: array
        An array of the natural-log priors
    resume_file: string
        filepath for the resume file which stores the weights
    n_checkpoint: int
        Number of samples to reweight before writing a resume file
    """
    from tqdm.auto import tqdm

    nposterior = len(result.posterior)

    if (resume_file is not None) and os.path.exists(resume_file):
        (
            old_log_likelihood_array,
            old_log_prior_array,
            new_log_likelihood_array,
            new_log_prior_array,
        ) = np.genfromtxt(resume_file)

        starting_index = np.argmin(np.abs(old_log_likelihood_array))
        logger.info(f"Checkpoint resuming from {starting_index}.")

    else:
        old_log_likelihood_array = np.zeros(nposterior)
        old_log_prior_array = np.zeros(nposterior)
        new_log_likelihood_array = np.zeros(nposterior)
        new_log_prior_array = np.zeros(nposterior)

        starting_index = 0

    dict_samples = [
        {key: sample[key] for key in result.posterior}
        for _, sample in result.posterior.iterrows()
    ]
    n = len(dict_samples) - starting_index

    keys = result.search_parameter_keys.copy()

    if result._meta_data["likelihood"]["time_marginalization"]:
        keys.append("geocent_time")
    if result._meta_data["likelihood"]["phase_marginalization"]:
        keys.append("phase")
    if result._meta_data["likelihood"]["distance_marginalization"]:
        keys.append("luminosity_distance")

    # Helper function to compute likelihoods in parallel
    def eval_pool(this_logl):
        with multiprocessing.Pool(processes=npool) as pool:
            chunksize = max(100, n // (2 * npool))
            return list(
                tqdm(
                    pool.imap(
                        partial(__eval_l, this_logl),
                        dict_samples[starting_index:],
                        chunksize=chunksize,
                    ),
                    desc="Computing likelihoods",
                    total=n,
                )
            )

    if old_likelihood is None:
        old_log_likelihood_array[starting_index:] = result.posterior["log_likelihood"][
            starting_index:
        ].to_numpy()
    else:
        old_log_likelihood_array[starting_index:] = eval_pool(old_likelihood)

    if new_likelihood is None:
        # Don't perform likelihood reweighting (i.e. likelihood isn't updated)
        new_log_likelihood_array[starting_index:] = old_log_likelihood_array[
            starting_index:
        ]
    else:
        new_log_likelihood_array[starting_index:] = eval_pool(new_likelihood)

    # Compute priors
    for ii, sample in enumerate(
        tqdm(dict_samples[starting_index:], desc="Computing priors", total=n),
        start=starting_index,
    ):
        if old_prior is not None:
            old_log_prior_array[ii] = old_prior.ln_prob(
                {key: sample[key] for key in keys}
            )
        else:
            old_log_prior_array[ii] = sample["log_prior"]

        if new_prior is not None:
            new_log_prior_array[ii] = new_prior.ln_prob(
                {key: sample[key] for key in keys}
            )
        else:
            # Don't perform prior reweighting (i.e. prior isn't updated)
            new_log_prior_array[ii] = old_log_prior_array[ii]

        if (ii % (n_checkpoint) == 0) and (resume_file is not None):
            checkpointed_index = np.argmin(np.abs(old_log_likelihood_array))
            logger.info(f"Checkpointing with {checkpointed_index} samples")
            np.savetxt(
                resume_file,
                [
                    old_log_likelihood_array,
                    old_log_prior_array,
                    new_log_likelihood_array,
                    new_log_prior_array,
                ],
            )

    ln_weights = (
        new_log_likelihood_array
        + new_log_prior_array
        - old_log_likelihood_array
        - old_log_prior_array
    )

    return (
        ln_weights,
        new_log_likelihood_array,
        new_log_prior_array,
        old_log_likelihood_array,
        old_log_prior_array,
    )


def rejection_sample(posterior, weights) -> Any:
    """Perform rejection sampling on a posterior using weights

    Parameters
    ==========
    posterior: pd.DataFrame or np.ndarray of shape (nsamples, nparameters)
        The dataframe or array containing posterior samples
    weights: np.ndarray
        An array of weights

    Returns
    =======
    reweighted_posterior: pd.DataFrame
        The posterior resampled using rejection sampling

    """
    keep = weights > np.random.uniform(0, max(weights), weights.shape)
    return posterior[keep]


def reweight(
    result,
    label=None,
    new_likelihood=None,
    new_prior=None,
    old_likelihood=None,
    old_prior=None,
    conversion_function=None,
    npool=1,
    verbose_output=False,
    resume_file=None,
    n_checkpoint=5000,
    use_nested_samples=False,
) -> tuple:
    """Reweight a result to a new likelihood/prior using rejection sampling

    Parameters
    ==========
    label: str, optional
        An updated label to apply to the result object
    new_likelihood: bilby.core.likelood.Likelihood, (optional)
        If given, the new likelihood to reweight too. If not given, likelihood
        reweighting is not applied
    new_prior: bilby.core.prior.PriorDict, (optional)
        If given, the new prior to reweight too. If not given, prior
        reweighting is not applied
    old_likelihood: bilby.core.likelihood.Likelihood, (optional)
        If given, calculate the old likelihoods from this object. If not given,
        the values stored in the posterior are used.
    old_prior: bilby.core.prior.PriorDict, (optional)
        If given, calculate the old prior from this object. If not given,
        the values stored in the posterior are used.
    conversion_function: function, optional
        Function which adds in extra parameters to the data frame,
        should take the data_frame, likelihood and prior as arguments.
    npool: int, optional
        Number of threads with which to execute the conversion function
    verbose_output: bool, optional
        Flag determining whether the weight array and associated prior and
        likelihood evaluations are output as well as the result file
    resume_file: string, optional
        filepath for the resume file which stores the weights
    n_checkpoint: int, optional
        Number of samples to reweight before writing a resume file
    use_nested_samples: bool, optional
        If true reweight the nested samples instead. This can greatly improve reweighting efficiency, especially if the
        target distribution has support beyond the proposal posterior distribution.

    Returns
    =======
    result: bilby.core.result.Result
        A copy of the result object with a reweighted posterior
    new_log_likelihood_array: array, optional (if verbose_output=True)
        An array of the natural-log likelihoods from the new likelihood
    new_log_prior_array: array, optional (if verbose_output=True)
        An array of the natural-log priors from the new likelihood
    old_log_likelihood_array: array, optional (if verbose_output=True)
        An array of the natural-log likelihoods from the old likelihood
    old_log_prior_array: array, optional (if verbose_output=True)
        An array of the natural-log priors from the old likelihood

    """
    from scipy.special import logsumexp

    result = copy(result)

    if use_nested_samples:
        result.posterior = result.nested_samples

    nposterior = len(result.posterior)
    logger.info("Reweighting posterior with {} samples".format(nposterior))

    (
        ln_weights,
        new_log_likelihood_array,
        new_log_prior_array,
        old_log_likelihood_array,
        old_log_prior_array,
    ) = get_weights_for_reweighting(
        result,
        new_likelihood=new_likelihood,
        new_prior=new_prior,
        old_likelihood=old_likelihood,
        old_prior=old_prior,
        resume_file=resume_file,
        n_checkpoint=n_checkpoint,
        npool=npool,
    )

    if use_nested_samples:
        ln_weights += np.log(result.posterior["weights"])

    weights = np.exp(ln_weights)

    # Overwrite the likelihood and prior evaluations
    result.posterior["log_likelihood"] = new_log_likelihood_array
    result.posterior["log_prior"] = new_log_prior_array

    result.posterior = rejection_sample(result.posterior, weights=weights)
    result.posterior = result.posterior.reset_index(drop=True)
    logger.info(
        "Rejection sampling resulted in {} samples".format(len(result.posterior))
    )
    result.meta_data["reweighted_using_rejection_sampling"] = True

    if use_nested_samples:
        result.log_evidence += np.log(np.sum(weights))
    else:
        result.log_evidence += logsumexp(ln_weights) - np.log(nposterior)

    if new_prior is not None:
        for key, prior in new_prior.items():
            result.priors[key] = prior

    if conversion_function is not None:
        data_frame = result.posterior
        if "npool" in inspect.signature(conversion_function).parameters:
            data_frame = conversion_function(
                data_frame, new_likelihood, new_prior, npool=npool
            )
        else:
            data_frame = conversion_function(data_frame, new_likelihood, new_prior)
        result.posterior = data_frame

    if label:
        result.label = label
    else:
        result.label += "_reweighted"

    if verbose_output:
        return (
            result,
            weights,
            new_log_likelihood_array,
            new_log_prior_array,
            old_log_likelihood_array,
            old_log_prior_array,
        )
    else:
        return result
