"""
Some constant for executions :

* ``N`` : BORG number of points per axis

* ``gridsize`` : BORG box size 

* ``nside`` : healpy parameter
"""

N = 256
boxsize = 677.7
nside = 128
fwhm = 0.03

from matplotlib.markers import MarkerStyle
from matplotlib.transforms import Affine2D

small_dot = MarkerStyle("o", transform=Affine2D().scale(0.2))

from itertools import chain
import matplotlib.colors as mcolors
from cycler import cycler

best_cycle = cycler(
    color=chain.from_iterable(
        zip(
            list(filter(lambda x: "blue" in x, mcolors.XKCD_COLORS.keys())),
            list(filter(lambda x: "green" in x, mcolors.XKCD_COLORS.keys())),
        )
    )
)


from astropy import cosmology
from numpy import linspace
from scipy.interpolate import interp1d

H0 = 67.66  # Hubble rate in km/s/Mpc
Om0 = 0.3111  # Matter density parameter
Ode0 = 1.0 - Om0  # Dark energy density parameter:
cosmo = cosmology.LambdaCDM(H0, Om0, Ode0)

comov_to_ld = interp1d(
    cosmo.comoving_distance(linspace(0, 0.5, 100000)) * cosmo.h,
    cosmo.luminosity_distance(linspace(0, 0.5, 100000)),
    kind="quadratic",
    copy=True,
    bounds_error=True,
)

ld_to_comov = interp1d(
    cosmo.luminosity_distance(linspace(0, 0.5, 100000)),
    cosmo.comoving_distance(linspace(0, 0.5, 100000)) * cosmo.h,
    kind="quadratic",
    copy=True,
    bounds_error=True,
)
