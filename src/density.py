"""
Extraction of the density of dark matter around SNe
"""

import numpy as np
from src.const import boxsize
from scipy.spatial._ckdtree import cKDTree
from astropy import units as u
from astropy.table import QTable
from astropy.units import Quantity


def density_BORG(
    points_coord: np.ndarray | Quantity,
    tree: cKDTree,
    den_r: np.ndarray,
    shift: np.ndarray = np.array([boxsize / 2] * 3),
    k: int = 1,
) -> np.ndarray:
    """
    Extract the density of dark matter around points using the BORG data grid.

    Parameters
    ----------
    points_coord : (n, 3) np.ndarray
        Coordinates of the points
    tree : scipy.spatial.cKDTree
        Tree of the points
    den_r : np.ndarray
        Densities at each point in the box
    k : int, default 1
        Number of nearest neighbour to consider. If > 1, it will average over the neighbours.

    Return
    ------
    density_SN : (n, 1) np.ndarray
        Density at each point
    """
    try:
        dist_id, nn_id = tree.query(points_coord + shift, k=k)
    except u.UnitConversionError:
        dist_id, nn_id = tree.query(points_coord.value + shift, k=k)

    if k == 1:
        density = den_r[nn_id]
    else:
        density = np.average(den_r[nn_id], axis=1, weights=1 / dist_id)

    return density


def points_above_threshold(
    points_coord: np.ndarray,
    tree: cKDTree,
    den_r: np.ndarray,
    threshold: float = 0,
    k: int = 8,
) -> np.ndarray:
    """
    Return an array indicating whether the points are above the density threshold or not.

    Parameters
    ----------
    points_coord : (n, 3) np.ndarray
        Coordinates of the points
    tree : scipy.spatial.cKDTree
        Tree of the points
    den_r : np.ndarray
        Densities at each point in the box
    threshold : float, default 0
        Threshold above which to cut
    k : int, default 1
        Number of nearest neighbour to consider. If > 1, it will average over the neighbours.

    Return
    ------
    above : (n, 1) np.ndarray
        Each index is ``True``if the density at the point coordinates is above the threshold, ``False`` otherwise
    """
    return density_BORG(points_coord, tree, den_r, k=k) > threshold


def points_in_dm_smoothed(
    points_coord: np.ndarray,
    tree: cKDTree,
    den_r: np.ndarray,
    shift: float = -0.3,
    alpha: float = 4.5,
    k: int = 8,
) -> np.ndarray:
    """
    Return an array indicating whether the points are kept or not, using a sigmoid (logistic) probability distribution.

    Parameters
    ----------
    points_coord : (n, 3) np.ndarray
        Coordinates of the points
    tree : scipy.spatial.cKDTree
        Tree of the points
    den_r : np.ndarray
        Densities at each point in the box
    shift : float, default 0.5
        Shift applied to the sigmoid. The shift point has a 0.5 probability to be kept
    alpha : float, default 4.5
        Steepness of the logistic function.
    k : int, default 1
        Number of nearest neighbour to consider. If > 1, it will average over the neighbours.

    Return
    ------
    above : (n, 1) np.ndarray
        Each index is ``True``if the density at the point coordinates is kept, ``False`` otherwise
    """

    densities = density_BORG(points_coord, tree, den_r, k=k)
    proba = 1 / (1 + np.exp(-(densities - shift) * alpha))
    return np.random.random(proba.shape) < proba


def density_posterior(
    candidates: np.ndarray | Quantity, tree: cKDTree, density: np.ndarray, k: int = 1
) -> np.ndarray:
    """
    Extract the density of posterior points around candidates positions using the posterior histogramed tree.

    Parameters
    ----------
    points_coord : (n, 3) np.ndarray
        Coordinates of the candidates points
    tree : scipy.spatial.cKDTree
        Tree of the posterior density points
    count : np.ndarray
        Densities of posterior points, has same indexing at the points in the tree.
    k : int, default 1
        Number of nearest neighbour to consider. If > 1, it will average over the neighbours.

    Return
    ------
    density : (n, 1) np.ndarray
        Posterior density at each candidate point
    """
    try:
        dist_id, nn_id = tree.query(candidates, k=k)
    except u.UnitConversionError:
        dist_id, nn_id = tree.query(candidates.value, k=k)
    try:
        density = np.average(density[nn_id], axis=1, weights=1 / dist_id)
    except np.AxisError:
        density = density[nn_id]

    return density


def good_candidates(
    candidate_table: QTable, posterior: QTable, bins: int = 200
) -> None:
    """
    Evaluates the candidates by taking the density of posterior points around them.
    This function doesn't return anything, but adds a `posterior_density` column to the candidates table.

    Parameters
    ----------
    candidate_table : astropy.QTable
        Table of the candidates
    posterior : astropy.QTable
        Table of the posterior points
    bins : int or array-like
        Binning to use when histrogramming the posterior
    """
    count, binsra, binsdec = np.histogram2d(
        posterior["ra"], posterior["dec"], bins=bins
    )
    middlera, middledec = (binsra[:-1] + binsra[1:]) / 2, (
        binsdec[:-1] + binsdec[1:]
    ) / 2

    radec_tree = cKDTree(
        np.dstack(
            np.meshgrid(middlera.to(u.rad).value, middledec.to(u.rad).value + np.pi / 2)
        ).reshape(-1, 2)
    )

    candidates = np.vstack(
        [candidate_table["ra"].to(u.rad).value, candidate_table["dec"].to(u.rad).value]
    ).T

    candidate_table["posterior_density"] = density_posterior(
        candidates, radec_tree, count.T.flatten(), k=10
    )
