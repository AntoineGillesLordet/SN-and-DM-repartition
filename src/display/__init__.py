from .mollweide import *
from .view_3d import *
from .histograms import *
from .corner import *
