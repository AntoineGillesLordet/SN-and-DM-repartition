"""
Corner plot for posteriors, focusing on RA/DEC/luminosity distance
"""

import numpy as np
import corner

from matplotlib.figure import Figure
from astropy.table import QTable
from astropy.units import Quantity, Unit, rad, Mpc
from typing import Any


def plot_corner_simu(
    posterior: QTable, units: dict[str, Unit] = {"angle": rad, "dist": Mpc}, **kwargs
) -> Figure | None:
    """
    Corner plot of a posterior extracted from a skymap.
    Has only informations in ra, dec and luminosity_distance.

    Parameters
    ----------
    posterior : astropy.QTable
        Table of the posterior points
    units : dict, optional
        Units to use for the plot. Default is radians for angles, Mpc for distances.
    **kwargs : Any
        All kwargs are passed to ``corner.corner``

    Return
    ------
    fig : matplotlib.figure.Figure
        Figure of the corner plot if it wasn't provided with the ``fig`` kwargs
    """
    default_params = dict(
        show_titles=True,
        bins=50,
        smooth=0.9,
        label_kwargs=dict(fontsize=16),
        title_kwargs=dict(fontsize=16),
        color="b",
        title_quantiles=[0.16, 0.5, 0.84],
        quantiles=[0.16, 0.84],
        levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.0)),
        plot_density=False,
        plot_datapoints=True,
        fill_contours=True,
        max_n_ticks=7,
        hist_kwargs=dict(density=True, color="b"),
        labels=[r"RA", r"DEC", r"$l_d$"],
        alpha=0.2,
        fig=None,
    )

    default_params["hist_kwargs"].update(kwargs.pop("hist_kwargs", {}))
    title = kwargs.pop("title", None)
    default_params.update(**kwargs)

    if "color" in kwargs:
        default_params["hist_kwargs"]["color"] = kwargs["color"]

    fig = corner.corner(
        np.array(
            [
                posterior["ra"].to(units["angle"]),
                posterior["dec"].to(units["angle"]),
                posterior["luminosity_distance"].to(units["dist"]),
            ]
        ).T,
        **default_params,
    )
    fig.set(figheight=20, figwidth=20)
    if title:
        fig.suptitle(title, y=1.01, fontsize=20)

    if default_params["fig"] is None:
        return fig
    else:
        return None


def plot_corner_candidates(
    fig: Figure,
    candidates: QTable,
    den_threshold: Any = 4,
    units: dict[str, Unit] = {"angle": rad, "dist": Mpc},
) -> None:
    """
    Plot the position of potential candidates for an event on a corner plot.
    Has only informations in ra, dec and luminosity_distance.

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        Figure of the corner plot
    candidates : astropy.QTable
        Table of the candidates, must at least have columns ``['ra', 'dec', 'luminosity_distance', 'posterior_density']``
    den_threshold : float or Quantity
        Threshold on the posterior density at the candidates positions to consider.
        Should have the same unit as ``candidates['posterior_density']``
    units : dict, optional
        Units to use for the plot. Default is radians for angles, Mpc for distances.
    """
    adim = candidates[(candidates["posterior_density"] > den_threshold)]

    points = np.array(
        [
            adim["ra"].to(units["angle"]).value,
            adim["dec"].to(units["angle"]).value,
            adim["luminosity_distance"].to(units["dist"]).value,
        ]
    ).T

    import matplotlib.colors as mcolors
    from cycler import cycler
    import itertools

    best_cycle = cycler(
        color=itertools.chain.from_iterable(
            zip(
                list(filter(lambda x: "yellow " in x, mcolors.XKCD_COLORS.keys())),
                list(filter(lambda x: "red " in x, mcolors.XKCD_COLORS.keys())),
                list(filter(lambda x: "green " in x, mcolors.XKCD_COLORS.keys())),
            )
        )
    )

    for i, style in zip(range(len(points)), best_cycle):
        corner.overplot_lines(fig, points[i], **style)
        corner.overplot_points(fig, points[i][None], marker="s", **style)

    fig.suptitle(
        f"Corner plots with candidates in regions with a posterior density\n of {den_threshold}\
                  points per squared degree : {len(adim)} candidates left ({len(candidates)} initially)",
        y=1.01,
        fontsize=20,
    )
    fig.set(figheight=20, figwidth=20)
