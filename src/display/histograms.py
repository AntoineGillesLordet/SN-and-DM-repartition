"""
Histogram plotting function
"""

import numpy as np
import scipy.signal

from matplotlib.ticker import FixedLocator

from astropy import units as u

from matplotlib.axes import Axes
from astropy.table import QTable
from astropy.units import Quantity


def density_hist(
    ax: Axes,
    density_DM: np.ndarray,
    density_SN: np.ndarray,
    legend: bool = False,
    **kwargs
) -> tuple:
    """
    Plot the DM density and DM at SN points density histograms in the provided axis

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        Axes to plot the histograms on
    map : np.ndarray
        Healpy map of the dark matter density
    density_SN : np.ndarray
        Dark matter density at the SN points
    legend : bool, default ``False``
        Show the legend on the axis

    Return
    ------
    (count, bins) : (np.ndarray, np.ndarray)
        Count and bins of the density_DM histogram
    """
    color = kwargs.pop("color", "yellowgreen")

    count, bins, _ = ax.hist(density_DM, histtype="step", **kwargs)
    ax.set_ylabel("Number of DM points", color="blue")
    ax.tick_params(axis="y", labelcolor="blue")

    ax_SN = ax.twinx()
    dens_count, _, _ = ax_SN.hist(density_SN, bins=bins, histtype="step", color=color)
    ax_SN.set_ylabel("Number of SN", color=color)
    ax_SN.tick_params(axis="y", labelcolor=color)

    ax_SN.vlines(
        np.mean(density_SN),
        0,
        np.max(dens_count),
        linestyles=":",
        colors="orange",
        label="Mean density for SN",
    )
    ax_SN.vlines(
        np.mean(density_DM),
        0,
        np.max(dens_count),
        linestyles=":",
        colors="r",
        label="Mean DM density",
    )
    ax_SN.vlines(
        np.median(density_SN),
        0,
        np.max(dens_count),
        linestyles="--",
        colors="orange",
        alpha=0.5,
        label="Median of the density for SN",
    )
    ax_SN.vlines(
        np.median(density_DM),
        0,
        np.max(dens_count),
        linestyles="--",
        colors="r",
        alpha=0.5,
        label="Median of the DM density",
    )
    ax_SN.legend()

    if legend:
        ax.legend()

    return count, bins


def plot_frac_hist(
    ax: Axes,
    bins: np.ndarray,
    frac_dm: np.ndarray,
    frac_SN: np.ndarray,
    r: float = None,
    dr: float = None,
    legend: bool = False,
    SN_type: str = "SNe",
    **kwargs
) -> None:
    """
    Plot the fraction of points of the histogram above a cut

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        Axes for the plots
    bins : numpy.ndarray
        Bins used for the histogram, defines the cut to apply
    frac_dm : float array
        Fraction of the dark matter points with a density above the cut
    frac_SN :
        Fraction of the SN with a corresponding dark matter density above the cut
    r : optional, float
        Radius of the shell, only for labelling
    dr : optional, float
        Thickness of the shell, only for labelling
    legend : bool, default ``False``
        If ``True``, display the legend on the axes
    """
    ax.set_xlabel(r"Density threshold ($\delta_{cut}$)")
    ax.set_ylabel("Fraction")
    ax.xaxis.set_minor_locator(FixedLocator(np.arange(-1, 5, 0.1)))
    ax.xaxis.grid(which="minor", color="lightgray")
    ax.xaxis.grid(which="major", color="gray")
    ax.yaxis.set_minor_locator(FixedLocator(np.arange(-0.2, 1.2, 0.02)))
    ax.yaxis.grid(which="minor", color="lightgray")
    ax.yaxis.grid(which="major", color="gray")
    color = kwargs.get("color", "yellowgreen")
    if r is not None and dr is not None:
        ax.set_title("Fraction in shell {} {}".format(r, dr))
        ax.plot(bins, frac_dm, label="Dark matter {},{}".format(r, dr))
        ax.plot(bins, frac_SN, color=color, label="SN {},{}".format(r, dr))
    else:
        ax.set_title("Fraction of the points with a density above the threshold")
        ax.plot(bins, frac_dm, label="Dark matter")
        ax.plot(bins, frac_SN, color=color, label=SN_type)

    if legend:
        ax.legend()


def hull_split_hist(
    ax: Axes,
    posterior: QTable,
    hull_split: list,
    posterior_col: str = "luminosity_distance",
    bins: int | np.ndarray = 100,
    labels: list = None,
    true_value: float | Quantity = None,
    unit: u.Unit | None = None,
) -> None:
    """
    Plot the bilby posterior distribution and the ones obtained by intersecting it with the dark matter distribution
    on the given axis.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        Axes for the histograms
    posterior : astropy.QTable
        Bilby posterior points
    hull_split : (bool array) list
        Which posterior points are in the different hulls.
    posterior_col : str, default 'luminosity_distance'
        Which value of the posterior to use
    bins : numpy.ndarray
        Bins used for the histogram
    labels : str list
        Labels to use for each hull
    true_value : float or astropy.Quantity
        Plot a vertical line at this value if provided
    unit : astropy.Unit
        Unit to use for the plot. If none are provided, use the unit of the posterior column.
    """
    if unit is None:
        unit = posterior[posterior_col].unit
    _, bins, _ = ax.hist(
        posterior[posterior_col].to(unit),
        bins=bins,
        histtype="step",
        color="red",
        label="Bilby posterior",
    )
    if type(hull_split) == list and len(hull_split) > 1:
        for i in range(len(hull_split) - 2, -1, -1):
            ax.hist(
                posterior[hull_split[i]][posterior_col].to(unit),
                bins=bins,
                histtype="step",
                label=str(np.round_(labels[i], decimals=1))
                if labels is not None
                else "",
            )
    else:
        ax.hist(
            posterior[hull_split][posterior_col].to(unit),
            bins=bins,
            histtype="step",
            label="intersection",
        )

    ax.set_title(
        "Posterior for {} before and after intersecting with the DM density grid".format(
            posterior_col
        )
    )

    if type(true_value) == float:
        ax.vlines(true_value, 0, ax.get_ylim()[1], colors="orange")
    elif true_value is not None:
        ax.vlines(true_value.to(unit), 0, ax.get_ylim()[1], colors="orange")

    ax.legend()


def plot_peaks_width(
    ax: Axes,
    counts: np.ndarray,
    bins: np.ndarray,
    prominence: float = 40,
    width: float = 2,
    rel_height: float = 0.5,
    n: int = 5,
    **kwargs
) -> None:
    """
    Plots the peaks width of some histogrammed data on an axis

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        Axes to plot on
    counts : numpy.ndarray
        Counts of the histogrammed data
    bins : numpy.ndarray
        Bins of the histogrammed data
    prominence : float, optional
        Required peak prominence
    width : float, optional
        Required minimal peak width, in number of bins
    rel_height : float, optional
        Relative height at which to plot the peak width
    n : int
        Order of the smoothing filter $\frac{n}{1 + z^{-1} + z^{-2} + \dots + z^{-(n-1)}}$  used,
    **kwargs : Any
        All kwargs are passed as styles to ``ax.hlines``
    """
    default_style = dict(linestyle=":", color="r")
    default_style.update(**kwargs)

    _, properties = scipy.signal.find_peaks(
        scipy.signal.filtfilt([1] * n, n, counts),
        prominence=prominence,
        width=width,
        rel_height=rel_height,
    )

    ax.hlines(
        properties["width_heights"],
        [bins[int(left) + 1] for left in properties["left_ips"]],
        [bins[int(right) + 1] for right in properties["right_ips"]],
        **default_style
    )
