"""
Mollweide projection plotting function
"""
from matplotlib import pyplot as plt
import numpy as np
import healpy

from astropy.units import rad

from src.const import nside, fwhm, small_dot

from src.shell_analysis import get_contour_levels, generate_map
from src.bilby_healpy_patch import projview

from scipy.ndimage import gaussian_filter

from astropy.table import QTable
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from typing import Any


def plot_all_mollweide(
    shell_positions: np.ndarray,
    shell_den_r: np.ndarray,
    gal_df: QTable | None = None,
    SN_df: QTable | None = None,
    CCSN_df: QTable | None = None,
    interest_points: np.ndarray | None = None,
    fig: Figure | None = None,
    nside: int = nside,
    smooth: bool = True,
    fwhm: float = fwhm,
    title: str = None,
    savename: str = None,
    **kwargs
) -> None:
    """
    Display or save as a .png datas in a MollWeide projection. Datas are at least the dark matter density, and may include galaxy, SN, and CCSN positions.

    Parameters
    ----------
    shell_positions : np.ndarray
        Positions of the dark matter points to consider. Should have a shape (n,3), n being the number of points
    shell_den_r : np.ndarray
        Corresponding densities at each point
    gal_df : astropy.QTable, optional
        Galaxy table, plotted as small blue dots
    SN_df : astropy.QTable, optional
        SuperNovae table, plotted as green stars
    CCSN_df : astropy.QTable, optional
        Core Collapse SuperNovae only table, plotted as light blue crosses.
    interest_points : tuple or np.ndarray, optional
        Other points of interest, given as (RA, Dec)
    fig : matplotlib.figure.Figure, optional
        Figure to plot on. If not provided, create one. WILL CREATE AXIS IN IT EITHER WAY because of ``healpy``.
    nside : int
        nside parameter for healpy. Default value is set in ``const.py``
    smooth : bool, default True
        If True, smooth the map using ``healpy.sphtfunc.smoothing``
    fwhm : float, optional
        The full width half max parameter of the Gaussian (in radians) for the smoothing
    name : str, optional
        Name to display at the top of the plot. If ``None``, it will be 'Mollweide view'
    savename : str, optional
        If provided, saves the plot as a .png file. Otherwise, show it
    **kwargs : Any
        Kwargs will be passed to `healpy.projview`
    """

    if fig is None:
        fig = plt.figure(figsize=(16, 12))
    axhealpy = fig.add_subplot()

    plt.axes(axhealpy)
    mapp, inc = generate_map(
        shell_positions.T,
        shell_den_r,
        representation_type="cartesian",
        positive=True,
        nside=nside,
        smooth=smooth,
        fwhm=fwhm,
    )

    projview(
        np.log(mapp + inc),
        fig=fig,
        norm="hist",
        cmap="gist_yarg",
        graticule=True,
        graticule_labels=True,
        projection_type="mollweide",
        hold=True,
        reuse_axes=True,
        title=title,
        unit=r"$\log (\delta + {} )$".format(inc),
        **kwargs
    )

    if gal_df is not None:
        healpy.newprojplot(
            np.pi / 2 * rad - gal_df["dec"],
            gal_df["ra"].to(rad),
            marker=small_dot,
            color="b",
            linestyle="",
        )

    if SN_df is not None:
        healpy.newprojplot(
            np.pi / 2 * rad - SN_df["dec"],
            SN_df["ra"].to(rad),
            marker="*",
            color="greenyellow",
            linestyle="",
        )

    if CCSN_df is not None:
        healpy.newprojplot(
            np.pi / 2 * rad - CCSN_df["dec"],
            CCSN_df["ra"].to(rad),
            marker="x",
            color="aqua",
            linestyle="",
        )

    if interest_points is not None:
        try:
            healpy.newprojplot(
                np.pi / 2 * rad - interest_points[:, 1],
                interest_points[:, 0]
                - 2 * np.pi * rad * (interest_points[:, 0] > np.pi * rad),
                marker="o",
                color="blue",
                linestyle="",
            )
        except IndexError:
            healpy.newprojplot(
                np.pi / 2 * rad - interest_points[1],
                interest_points[0]
                - 2 * np.pi * rad * (interest_points[0] > np.pi * rad),
                marker="o",
                color="blue",
                linestyle="",
            )

    if savename is not None:
        plt.savefig(savename)
    elif fig is None:
        plt.show()


def contourf_mollweide(
    posterior: QTable,
    fig: Figure = None,
    quantiles: np.ndarray | None = None,
    cmap: str = "Reds",
    fwhm: float = 0.09,
    nside: int = 256,
    **kwargs
) -> None:
    """
    Plot the filled contours of a 2D histogram on a healpy projection

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    ax : matplotlib.axes.Axes, optional
        Axes to plot on. If not provided, create one.
    levels : array, optional
        The values of the contour levels. Default is (1 sigma, 2 sigma, 3 sigma).
    cmap : str
        Colormap to use
    """

    not_provided = fig is None
    if not_provided:
        fig = plt.figure(figsize=(8, 5))

    map, lvls = get_contour_levels(
        posterior, nside=nside, quantiles=quantiles, fwhm=fwhm
    )

    map_ = np.zeros_like(map)
    for i, lvl in enumerate(lvls):
        map_ += (len(lvls) - i) ** 2 * (map > lvl)

    projview(
        map_,
        fig=fig,
        projection_type="mollweide",
        cmap=cmap,
        reuse_axes=True,
        hold=True,
        cbar=False,
        graticule_labels=True,
        graticule=True,
        **kwargs
    )


def contour_mollweide(
    posterior: QTable,
    ax: Axes = None,
    quantiles: np.ndarray | None = None,
    cmap: str = "viridis",
    fwhm: float = 0.09,
    nside: int = 256,
) -> None:
    """
    Plot the contours of a 2D histogram on a healpy projection, using the matplotlib `contour` function

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    ax : matplotlib.axes.Axes, optional
        Axes to plot on. If not provided, create one.
    sigma : float, optional
        Size of the gaussian blur to smooth the histogram.
    levels : array, optional
        The values of the contour levels. Default is (1 sigma, 2 sigma, 3 sigma).
    bins : int or np.ndarray, optional
        Bins to use for histogramming before getting the contours.
    sky_ticks : bool, default False
        Format the ticks to hours for RA
    cmap : str
        Colormap to use
    """
    not_provided = ax is None
    if not_provided:
        fig, ax = plt.subplots(figsize=(10, 10), subplot_kw={"projection": "mollweide"})

    map, lvls = get_contour_levels(
        posterior, nside=nside, quantiles=quantiles, fwhm=fwhm
    )

    bins_ra = np.linspace(-np.pi, np.pi, 301)
    bins_dec = np.linspace(-np.pi / 2, np.pi / 2, 301)
    middle_ra, middle_dec = (bins_ra[:-1] + bins_ra[1:]) / 2, (
        bins_dec[:-1] + bins_dec[1:]
    ) / 2

    middle_ra = (
        np.concatenate(
            [
                middle_ra[0] + np.array([-2, -1]) * np.diff(middle_ra[:2]),
                middle_ra,
                middle_ra[-1] + np.array([1, 2]) * np.diff(middle_ra[-2:]),
            ]
        )
        * rad
    )
    middle_dec = (
        np.concatenate(
            [
                middle_dec[0] + np.array([-2, -1]) * np.diff(middle_dec[:2]),
                middle_dec,
                middle_dec[-1] + np.array([1, 2]) * np.diff(middle_dec[-2:]),
            ]
        )
        * rad
    )

    expand_count = np.zeros((304, 304))

    for lvl in lvls:
        coords = healpy.pix2ang(nside=nside, ipix=np.where(map > lvl))
        theta, phi = coords * rad
        theta, phi = theta.flatten(), phi.flatten()
        ra, dec = (
            phi - 2 * np.pi * rad * (phi > np.pi * rad),
            np.pi / 2 * rad - theta,
        )
        count, _, _ = np.histogram2d(
            ra, dec, bins=300, range=[[-np.pi, np.pi], [-np.pi / 2, np.pi / 2]] * rad
        )
        expand_count += count.min() > 0
        expand_count[2:-2, 2:-2] += count > 0
        expand_count[2:-2, 1] += count[:, 0] > 0
        expand_count[2:-2, -2] += count[:, -1] > 0
        expand_count[1, 2:-2] += count[0] > 0
        expand_count[-2, 2:-2] += count[-1] > 0
        expand_count[1, 1] += count[0, 0] > 0
        expand_count[1, -2] += count[0, -1] > 0
        expand_count[-2, 1] += count[-1, 0] > 0
        expand_count[-2, -2] += count[-1, -1] > 0
        expand_count *= 1.5

    ax.contour(
        -middle_ra,
        middle_dec,
        gaussian_filter(expand_count.T, sigma=0.2),
        levels=len(lvls) - 1,
        cmap=cmap,
    )
