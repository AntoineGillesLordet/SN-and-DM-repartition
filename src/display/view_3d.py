"""
3d view plotting function
"""

import matplotlib.pyplot as plt
import numpy as np
from cycler import cycler
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from astropy import units as u
from scipy.spatial import cKDTree
from scipy.ndimage import gaussian_filter

from src.const import small_dot, boxsize
from src.volume_search import successive_shells
from src.utils import get_posterior_axis
from src.density import density_posterior

from matplotlib.axes import Axes
from astropy.table import QTable
from trimesh import Trimesh
from typing import Any
from numpy.typing import NDArray
from astropy.units import Unit


def split_posterior(
    posterior: QTable,
    meshes: list[Trimesh] | Trimesh | None = None,
    quantiles: Any = None,
    bins: int | NDArray = 50,
    unit: Unit = u.Mpc,
) -> list | Any:
    """
    Returns the posterior points contained by each hull.
    By default it uses the 1, 2 and 3 sigma hulls of the posterior.
    Note that `in_hull[-1]` contains the points that are in none of the hulls defined by levels.

    It also work for a single level, in which case it returns only a single boolean array.

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    meshes : trimesh.Trimesh list, optional
        List of hulls to use. If `None`, uses ``successive_shells`` to extract them.
    quantiles : float or array like, optional
        Quantiles of the density at which the shells are taken
    proj : str, optional
        Projection to use, `radec` or `cart`. Default is cartesian
    bins : int or NDArray
        Bins to use for histogramming
    units : dict, optional
        Units to use for the histogramming. Default is radians for angles, Mpc for distances.

    Return
    ------
    in_hull : bool list or bool array list
        `in_hull[i][j]` is `True` if the j-th point of the posterior is in the i-th hull.
        In case of a single level, `in_hull[i]` is `True` if the i-th point is in the hull.
    """
    points_coord = np.vstack(
        (
            posterior["x"].to(unit),
            posterior["y"].to(unit),
            posterior["z"].to(unit),
        )
    ).T

    if meshes is None:
        meshes = successive_shells(posterior, quantiles=quantiles, bins=bins, unit=unit)

    if isinstance(meshes, list):
        in_hull = [meshes[0].contains(points_coord)]
        for mesh in meshes[1:]:
            in_hull.append(mesh.contains(points_coord))
        in_hull.append(~np.any(in_hull, axis=0))
        return in_hull
    elif isinstance(meshes, Trimesh):
        return meshes.contains(points_coord)
    else:
        raise TypeError(f"Unsuported type {type(meshes)} for meshes")


def display_hull(
    posterior: QTable,
    quantiles: Any = None,
    ax: Any = None,
    proj: str = "cart",
    bins: int | NDArray = 50,
    units: dict[str, Unit] = {"angle": u.rad, "dist": u.Mpc},
) -> None:
    """
    Display the scatter plot of a posterior, where points are colored relative to the sigma region in which they land.
    Red points, orange points, and yellow points are respectively in the 1, 2 and 3 sigma regions
    while blue points are outside the 3 sigma region.

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    quantiles : float or array like, optional
        Quantiles of the density at which the shells are taken. Default cuts are at a 1, 2 and 3 sigma deviation
        from the peak density assuming a gaussian distribution
    ax : matplotlib.axes.Axes
        Axes on which to plot
    proj : ``'cart'`` or ``'radec'``
        Projection to use for histogramming and plotting
    bins : int or NDArray
        Bins to use for histogramming
    units : dict, optional
        Units to use for the plot. Default is radians for angles, Mpc for distances.

    """
    not_provided = ax is None

    if not_provided:
        fig, ax = plt.subplots(figsize=(15, 10), subplot_kw={"projection": "3d"})

    posterior_c1, posterior_c2, posterior_c3 = get_posterior_axis(
        posterior, proj=proj, units=units
    )
    in_hull = split_posterior(posterior, quantiles=quantiles, bins=bins, unit=units)
    nb_hull = len(in_hull) - 1

    for i, sty in zip(
        range(nb_hull),
        cycler("color", plt.cm.autumn(np.linspace(0, 1, nb_hull)))
        + cycler("alpha", np.linspace(0.8, 0.4, nb_hull)),
    ):
        if i == 0:
            ax.scatter(
                posterior_c1[in_hull[i]],
                posterior_c2[in_hull[i]],
                posterior_c3[in_hull[i]],
                **sty,
            )
        else:
            new_points = in_hull[i] & ~np.any(in_hull[: i - 1], axis=0)
            ax.scatter(
                posterior_c1[new_points],
                posterior_c2[new_points],
                posterior_c3[new_points],
                **sty,
            )

    ax.scatter(
        posterior_c1[in_hull[-1]],
        posterior_c2[in_hull[-1]],
        posterior_c3[in_hull[-1]],
        color="blue",
    )

    if not_provided:
        plt.show()


def plot_trimesh(
    trim: Trimesh, ax: Any = None, normals: bool = False, **kwargs
) -> None:
    """
    Plot a triangular mesh on a matplotlib 3d axes instance. If none are given, create a new figure.

    Parameters
    ----------
    trim : trimesh.Trimesh
        The triangular mesh to plot
    ax : matplotlib.axes.Axes
        The axes to plot on
    normals : bool, default False
        Show the normals to the faces.
    **kwargs : Any
        Any kwargs will be passed to `Poly3DCollection`
    """
    not_provided = ax is None
    if not_provided:
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, projection="3d")
    mesh = Poly3DCollection(trim.vertices[trim.faces], **kwargs)
    mesh.set_edgecolor("grey")
    mesh.set_alpha(kwargs.get("alpha", 0.5))
    ax.add_collection3d(mesh)
    ax.scatter(*np.array(trim.vertices).T, marker=small_dot, **kwargs)

    if normals:
        ax.quiver3D(
            *trim.triangles_center.T,
            *trim.face_normals.T,
        )

    if not_provided:
        ax.set_xlim(trim.vertices[:, 0].min(), trim.vertices[:, 0].max())
        ax.set_ylim(trim.vertices[:, 1].min(), trim.vertices[:, 1].max())
        ax.set_zlim(trim.vertices[:, 2].min(), trim.vertices[:, 2].max())
        plt.show()


def plot_posterior(
    posterior: QTable,
    bins: Any = None,
    N_bins: int = 256,
    sigma: float = 4.0,
    ax: NDArray | None = None,
) -> None:
    if bins is None:
        bins = (np.linspace(-boxsize / 2, boxsize / 2, N_bins) * u.Mpc,) * 3

    x, y, z = np.meshgrid(
        (bins[0][:-1] + bins[0][1:]) / 2 - bins[0].min(),
        (bins[1][:-1] + bins[1][1:]) / 2 - bins[1].min(),
        (bins[2][:-1] + bins[2][1:]) / 2 - bins[1].min(),
    )
    tree = cKDTree(
        np.dstack((x.flatten(), y.flatten(), z.flatten())).reshape(-1, 3).value
    )

    count, _ = np.histogramdd(
        [posterior["x"], posterior["y"], posterior["z"]], bins=bins
    )
    count = gaussian_filter(count, sigma)
    pts = np.vstack(
        [
            posterior["x"] - bins[0].min(),
            posterior["y"] - bins[1].min(),
            posterior["z"] - bins[2].min(),
        ]
    ).value.T
    color_posterior = density_posterior(
        pts, tree, np.transpose(count, axes=[1, 0, 2]).flatten(), k=5
    )
    if ax is None:
        _, ax = plt.subplots(figsize=(10, 10), subplot_kw={"projection": "3d"})

    ax.scatter(posterior["x"], posterior["y"], posterior["z"], c=color_posterior, s=5)

    ax.azim = -45
    ax.elev = 30
