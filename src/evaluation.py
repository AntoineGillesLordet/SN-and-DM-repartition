"""
Evaluation of a 1D posterior quality
"""
import numpy as np
from numpy import ndarray, dtype
import healpy

from scipy.stats import gaussian_kde


from src.const import nside, boxsize, ld_to_comov
from src.shell_analysis import get_contour_levels
from src.utils import get_levels_from_quantiles

from astropy.table import QTable
from astropy.units import Unit, Quantity, Mpc, rad, degree
from astropy.coordinates import SkyCoord
from typing import Any

from src.volume_search import successive_shells


def get_radec_areas(
    posterior: QTable,
    quantiles: np.ndarray | None = None,
    nside: int = nside,
    fwhm: float = 0.09,
    degrees: bool = False,
) -> ndarray[Quantity, dtype[Quantity]]:
    """
    Get the areas of some regions of the posterior in the radec plane.
    Default are standard 1, 2 and 3 sigma regions from the maximum density.

    Parameters
    ----------
    posterior : QTable
        Posterior to consider
    quantiles : np.ndarray, optional
        Quantiles of the contours to extract
    nside : int, optional
        Healpy ``nside`` parameter to use for map generation
    fwhm : float, optional
        Smoothing parameter passed to ``scipy.ndimage.gaussian_filter``
    degrees : bool, optional
        If ``True``, areas are returned in deg^2, otherwise they are in rad^2. Default is rad^2

    Return
    ------
    areas : Quantity array
        The areas corresponding to each levels, in ``unit**2``
    """
    map, V = get_contour_levels(posterior, quantiles=quantiles, fwhm=fwhm)
    if degrees:
        return [
            np.sum(map > lvl) * healpy.nside2pixarea(nside, degrees=degrees)
            for lvl in V
        ] * degree**2
    else:
        return [np.sum(map > lvl) * healpy.nside2pixarea(nside) for lvl in V] * rad**2


def get_radec_ratios(
    posterior: QTable,
    restricted_posterior: QTable,
    quantiles: float | np.ndarray = (0.5, 0.9),
    unit: Unit = Mpc,
    slices_edges: Quantity = None,
) -> QTable:
    """
    Get the area ratios of some regions of a posterior compared to a restricted posterior.

    Parameters
    ----------
    posterior : QTable
        Posterior to use.
    restricted_posterior : QTable
        Restricted posterior to use.
    quantiles: float or array like
        Quantiles of the regions to extract
    unit: Unit, optional
        Length unit for distance
    slices_edges: Quantity,  optional
        Edges of shells to split the posterior. If not provided, the full posterior distance range is used.

    Return
    ------
    data : list
        List containing the total number of points in the shell for both posterior and the area ratios.
    """
    table_ratios = QTable(
        names=[
            "Slice inner",
            "Slice outer",
            "Nb points full",
            "Nb points restrict",
            *[f"{qtl}% region ratio" for qtl in quantiles],
        ],
        dtype=[float, float, int, int, *([float] * len(quantiles))],
        units=[unit, unit, None, None, *([None] * len(quantiles))],
        masked=True,
    )

    if slices_edges is None:
        min_dist = np.min(
            50,
            posterior["luminosity_distance"].min(),
            restricted_posterior["luminosity_distance"].min(),
        )
        max_dist = np.max(
            500,
            posterior["luminosity_distance"].max(),
            restricted_posterior["luminosity_distance"].max(),
        )
        slices_edges = np.array([min_dist, max_dist]) * unit

    slices_outer = slices_edges[1:]
    slices_inner = slices_edges[:-1]

    for inner, outer in zip(slices_inner, slices_outer):
        slice_ = posterior[
            (posterior["luminosity_distance"] > inner)
            & (posterior["luminosity_distance"] < outer)
        ]
        slice_r_ = restricted_posterior[
            (restricted_posterior["luminosity_distance"] > inner)
            & (restricted_posterior["luminosity_distance"] < outer)
        ]

        row = [
            inner,
            outer,
            len(slice_),
            len(slice_r_),
            *(
                get_radec_areas(slice_, quantiles=quantiles)
                / get_radec_areas(posterior=slice_r_, quantiles=quantiles)
            ),
        ]
        table_ratios.add_row(row)

    for qtl in quantiles:
        table_ratios[f"{qtl}% region ratio"].mask = np.isinf(
            table_ratios[f"{qtl}% region ratio"]
        ) | np.isnan(table_ratios[f"{qtl}% region ratio"])
    return table_ratios


def get_kde_levels(
    posterior: QTable,
    coord: str = "luminosity_distance",
    quantiles: np.ndarray | None = None,
    bw_method: float = 0.1,
    N_points: int = 1000,
) -> tuple:
    """
    Get the levels at which to apply the cut on the posterior pdf for the given coordinate
    to get the corresponding quantiles contours.
    Default are gaussian 1, 2 and 3 sigma contours.

    Parameters
    ----------
    posterior : QTable
        Posterior to use
    coord : str, optional
        Axis of the posterior to use, default is ``'luminosity_distance'``
    quantiles : np.ndarray, optional
        Quantiles of the contours to extract.
    bw_method : float, optional
        Bandwidth method to use, passed to ``scipy.stats.gaussian_kde``.
    N_points : int, optional
        Number of points on which to evaluate the kde to get the levels.

    Return
    ------
    xx : Quantity array
        Values at which the kde has been evaluated, has shape ``(N_points,1)``.
    values : np.ndarray
        Values of the kde at those points, has shape ``(N_points,1)``.
    V : np.ndarray
        Thresholds associated with the quantiles.
    """
    if quantiles is None:
        quantiles = 1 - np.exp(-np.array([4.5, 2, 0.5]))

    span = posterior[coord].max() - posterior[coord].min()
    xx = np.linspace(
        posterior[coord].min() - 0.01 * span,
        posterior[coord].max() + 0.01 * span,
        N_points,
    )

    kde = gaussian_kde(posterior[coord], bw_method=bw_method)

    values = kde(xx)

    V = get_levels_from_quantiles(values, quantiles)

    return xx, values, V


def get_ld_range(
    posterior: QTable, quantiles: np.ndarray | None = None, unit: Unit = Mpc, **kwargs
) -> Quantity:
    """
    Get the total range covered by some quantiles in luminosity distance.

    Parameters
    ----------
    posterior : QTable
        Posterior to use.
    quantiles : np.ndarray, optional
        Quantiles of the contours to extract.
    unit : Unit, optional
        Unit of the obtained range, default is Mpc.

    Return
    ------
    ranges : Quantity array
        Total ranges covered by each quantile.
    """
    xx, values, V = get_kde_levels(posterior, quantiles=quantiles, **kwargs)
    step = np.diff(xx).mean()
    return np.array([np.sum(values > lvl) * step.to_value(unit) for lvl in V]) * unit


def get_volume_ratios(
    posterior: QTable,
    restricted_posterior: QTable,
    quantiles: np.ndarray | float,
    N_bins: int = 256,
    sigma: float = 4.0,
) -> ndarray:
    bins = (np.linspace(-boxsize / 2, boxsize / 2, N_bins) * Mpc,) * 3

    posterior_meshes = successive_shells(posterior, quantiles, bins=bins, sigma=sigma)

    restrict_meshes = successive_shells(
        restricted_posterior, quantiles, bins=bins, sigma=sigma
    )

    return np.array(
        [
            posterior_meshes[i].volume / restrict_meshes[i].volume
            for i, _ in enumerate(quantiles)
        ]
    )


def get_source_dist(posterior, coord) -> ndarray[Quantity, dtype[Quantity]]:
    posterior_coords = SkyCoord(
        ra=posterior["ra"],
        dec=posterior["dec"],
        distance=ld_to_comov(posterior["luminosity_distance"]) * Mpc,
    )
    distances = coord.separation_3d(posterior_coords)
    return np.array(
        [
            np.mean(distances).value,
            np.std(distances).value,
        ]
    )*Mpc


def get_dist_ratios(
    posterior: QTable, restricted_posterior: QTable, coord: SkyCoord
) -> ndarray:
    return get_source_dist(posterior, coord) / get_source_dist(
        restricted_posterior, coord
    )
