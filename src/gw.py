"""
Gravitational waves simulations
"""

import bilby
import numpy as np
from src.load.loading import read_dark_matter
import logging
import healpy
from scipy.stats import norm
from src.priors import BORGPriorDist, BORGPrior
from src.const import nside
from src.bilby_healpy_patch import HealPixMapPriorDist, HealPixPrior
from src.shell_analysis import extract_shell


def BNS(
    label: str = "BNS",
    outdir: str | None = None,
    mode: str = "default",
    sampler: str = "nestle",
    fixed: str | None = None,
    time_marginalization: bool = True,
    phase_marginalization: bool = True,
    distance_marginalization: bool = True,
    relative_binning: bool = False,
    npool: int = 8,
    **kwargs,
) -> str | None:
    print(f"Running in mode {mode} with label {label} and args {locals()}")

    # Set the duration and sampling frequency of the data segment that we're
    # going to inject the signal into
    duration = 32.0
    sampling_frequency = 2048.0
    minimum_frequency = 80.0

    # Specify the output directory and the name of the simulation.
    if outdir is None:
        outdir = f"outdir/{label}"

    bilby.core.utils.setup_logger(outdir=outdir, label=label)

    logger = logging.getLogger("bilby")

    if fixed is None:
        fixed = [
            "chi_1",
            "chi_2",
            "lambda_1",
            "lambda_2",
            "mass_1",
            "mass_2",
            "psi",
            "phase",
            "geocent_time",
            "mass_ratio",
            "chirp_mass",
        ]

    injection_parameters = dict(
        mass_1=1.5,
        mass_2=1.3,
        chi_1=0.02,
        chi_2=0.02,
        luminosity_distance=100.0,
        theta_jn=0.4,
        psi=2.659,
        phase=1.3,
        geocent_time=1126259642.413,
        ra=1.64,
        dec=-0.96,
        lambda_1=545,
        lambda_2=1346,
    )

    injection_parameters.update(kwargs)

    start_time = injection_parameters["geocent_time"] + 2 - duration

    waveform_arguments = dict(
        waveform_approximant="IMRPhenomPv2_NRTidalv2",
        reference_frequency=50.0,
        minimum_frequency=40.0,
    )

    # Setting the injection parameters
    if relative_binning:
        time_marginalization = False
        injection_parameters["fiducial"] = 1
        waveform_generator = bilby.gw.WaveformGenerator(
            duration=duration,
            sampling_frequency=sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star_relative_binning,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments,
        )
    else:
        waveform_generator = bilby.gw.WaveformGenerator(
            duration=duration,
            sampling_frequency=sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments,
        )

    # Setting the interferometers
    interferometers = bilby.gw.detector.InterferometerList(
        injection_parameters.pop("interferometer_list", ["H1", "L1"])
    )

    for interferometer in interferometers:
        interferometer.minimum_frequency = 40
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency, duration=duration, start_time=start_time
    )
    interferometers.inject_signal(
        parameters=injection_parameters, waveform_generator=waveform_generator
    )

    # Check if the event is detectable
    opt_SNR = np.linalg.norm(
        [interferometer.meta_data["optimal_SNR"] for interferometer in interferometers],
        ord=2,
    )

    if opt_SNR < 12:
        logger = logging.getLogger("bilby")
        logger.info(f"Optimal SNR {opt_SNR} is too low, aborting")
        return None, opt_SNR

    # Setting the prior
    priors = bilby.gw.prior.BNSPriorDict()
    m1 = injection_parameters["mass_1"]
    m2 = injection_parameters["mass_2"]
    injection_parameters["chirp_mass"] = (
        bilby.gw.conversion.component_masses_to_chirp_mass(m1, m2)
    )
    injection_parameters["mass_ratio"] = m2 / m1

    priors["luminosity_distance"] = bilby.gw.prior.UniformSourceFrame(
        50, 500, name="luminosity_distance"
    )

    for key in fixed:
        priors[key] = injection_parameters[key]

    if "geocent_time" in fixed:
        time_marginalization = False
    else:
        priors["geocent_time"] = bilby.core.prior.Uniform(
            minimum=injection_parameters["geocent_time"] - 0.1,
            maximum=injection_parameters["geocent_time"] + 0.1,
            name="geocent_time",
            latex_label="$t_c$",
            unit="$s$",
        )
    if "phase" in fixed:
        phase_marginalization = False

    if mode == "gauss":  # Gaussian patch in RA/DEC
        priors["dec"] = bilby.prior.Gaussian(
            mu=-1.2108,
            sigma=0.1,
            name="dec",
            latex_label="$\\mathrm{DEC}$",
            unit=None,
            boundary="reflective",
        )
        priors["ra"] = bilby.prior.Gaussian(
            mu=1.375,
            sigma=0.1,
            name="ra",
            latex_label="$\\mathrm{RA}$",
            unit=None,
            boundary="periodic",
        )

    elif mode == "BORG":  # Following the DM density from BORG
        priors_borg = BORGPriorDist("mcmc_10000.h5")
        priors["luminosity_distance"] = BORGPrior(priors_borg, "luminosity_distance")
        priors["ra"] = BORGPrior(priors_borg, "ra")
        priors["dec"] = BORGPrior(priors_borg, "dec")
        if distance_marginalization:
            logger.info(
                f"Distance marginalization not working with BORG, ignoring it..."
            )
            distance_marginalization = False

    elif mode == "hp_map":  # Following the DM density from a healpy map
        priordist = HealPixMapPriorDist(kwargs.pop("map", "test.fits"))
        priors["ra"] = HealPixPrior(priordist, "ra")
        priors["dec"] = HealPixPrior(priordist, "dec")
        priors["luminosity_distance"] = bilby.gw.prior.UniformSourceFrame(
            20, 500, name="luminosity_distance"
        )

    priors.validate_prior(duration, minimum_frequency)

    # Likelihood
    if relative_binning:
        likelihood = bilby.gw.likelihood.RelativeBinningGravitationalWaveTransient(
            interferometers=interferometers,
            waveform_generator=waveform_generator,
            time_marginalization=time_marginalization,
            phase_marginalization=phase_marginalization,
            distance_marginalization=distance_marginalization,
            priors=priors,
            fiducial_parameters=injection_parameters.copy(),
        )
    else:
        likelihood = bilby.gw.GravitationalWaveTransient(
            interferometers=interferometers,
            waveform_generator=waveform_generator,
            time_marginalization=time_marginalization,
            phase_marginalization=phase_marginalization,
            distance_marginalization=distance_marginalization,
            priors=priors,
        )

    # Sampling
    result = bilby.run_sampler(
        likelihood=likelihood,
        priors=priors,
        sampler=sampler,
        npoints=1000,
        injection_parameters=injection_parameters,
        outdir=outdir,
        label=label,
        conversion_function=bilby.gw.conversion.generate_all_bns_parameters,
        npool=npool,
    )

    result.plot_corner()
    return label, opt_SNR


def create_map(radius: float = 210, dr: float = 10) -> None:
    centroids, positions, den_r, tree = read_dark_matter("mcmc_10000.h5")
    _, shell_pos, shell_den = extract_shell(
        centroids=centroids,
        positions=positions,
        den_r=den_r,
        tree=tree,
        radius=radius,
        dr=dr,
    )

    def sigmoid(x: float | np.ndarray) -> float | np.ndarray:
        return 1 / (1 + np.exp(-4.5 * (x + 0.3)))

    map_ = np.zeros(healpy.nside2npix(nside))
    ipix = healpy.vec2pix(nside, *shell_pos.T)
    for i in range(len(ipix)):
        map_[ipix[i]] += sigmoid(shell_den[i])

    map_ = healpy.sphtfunc.smoothing(map_, fwhm=0.03)

    healpy.write_map("test.fits", map_, overwrite=True)


def create_unif_map() -> None:
    map_ = np.ones(healpy.nside2npix(nside))

    healpy.write_map("test_unif.fits", map_, overwrite=True)


if __name__ == "__main__":
    # -14_-54_-198 : ra = 4.44409348, dec = -1.29477994, luminosity_distance = 215.70520072
    # EDGE : ra = 5.843185307179586, dec = 0.28, luminosity_distance = 97.82875455238133
    # Void : ra = 5.783185307179586, dec = 0.5, luminosity_distance = 100
    BNS(
        label="BNS_BORG_rel_bin_more_-14_-54_-198",
        mode="BORG",
        sampler="dynesty",
        ra=4.44409348,
        dec=-1.29477994,
        luminosity_distance=215.70520072,
        relative_binning=True,
    )
