"""
Data loading functions.
Read datas from BORG h5 output for dark matter, weird 2M++ data files for galaxies, ZTF SN csv for SuperNovae.
"""
import os

import h5py
import numpy as np
import pandas
from bilby.core.result import read_in_result

from astropy.units import Mpc, rad, degree
from astropy.constants import c
from astropy.table import QTable

from scipy.spatial import cKDTree

from numpy.typing import NDArray

from src.load.utils import (
    gridListPermutation,
    complete_path,
    cut_grid,
    convertdec,
    convertra,
    complete_positions,
)
from src.load.scrapping import get_candidates
from src.const import N, boxsize, cosmo
from src.utils import get_posterior_axis
from src.density import points_in_dm_smoothed


def read_dark_matter(filename) -> tuple[NDArray, NDArray, NDArray, cKDTree]:
    """
    Read a h5 file containing the grid of dark matter and generates the corresponding
    centroids, posisitions and voxel densities. All returned arrays share the same indexing,
    i.e. the i-th point centroid coordinates are ``centroids[i]``, the point is at ``positions[i]``
    and it has a density ``den_r[i]``.

    Parameters
    ----------
    filename : str
        Path or name of the file

    Return
    ------
    centroids : (int list) list
        Coordinates of the centroids in the box, as points in a cubic mesh of size N
    positions : (float list) list
        Physical coordinates at each point considering the box is centered on [0, 0, 0]
    den_r : float list
        Densities at each point
    tree : scipy.spatial.cKDTree
        Useful for fast search of neighbouring points
    """
    sample = h5py.File(complete_path(filename), "r")
    # Get the density field:
    den = sample["scalars"]["BORG_final_density"][()]
    # Re-order the field to Fortran ordering:
    den_r256 = np.reshape(den, (N, N, N), order="F")  # As a (256,256,256) array
    den_r = np.reshape(den_r256, N**3)  # As a linear 256^3 array
    # Create a list of positions for each voxel:
    # Grid co-ordinates:
    grid = gridListPermutation(N, perm=(0, 1, 2))
    # Physical position of the centre of each voxel. These are relative to the
    # lower front left corner of the box:
    centroids = grid * boxsize / N + boxsize / (2 * N)
    # Positions relative to the centre of the box (which is where the Milky way
    # is assumed to lie). These should now be in Equatorial co-ordinates:
    positions = centroids - np.array([boxsize / 2] * 3)
    # KD tree of the positions, for convenience. Note that it only accepts
    # positive values, so we have to use the centroids!
    tree = cKDTree(centroids, boxsize=boxsize)
    return centroids, positions, den_r, tree


def read_galaxies(filename, only_in_grid=True) -> QTable:
    """
    Read the galaxy data from a weirdly formatted file

    Parameters
    ----------
    filename : str
        Path or name of the file to read
    only_in_grid : bool
        If True, only keep the galaxies that are in the same box as the dark matter data

    Return
    ------
    table : astropy.QTable
        Table containing the galaxies informations, in particular their spherical and cartesian coordinates
    """
    # Load the weirdly formatted galaxy data:
    table = QTable.from_pandas(
        pandas.read_csv(
            complete_path(filename),
            delimiter="|",
            header=24,
            skiprows=[30],
            usecols=(0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16),
            skipinitialspace=True,
        )
    )
    table = table[(table["ZoA"] == 0.0) & (table["Vh"] > 0)]
    table["redshift"] = table["Vh"] * 10**3/ c.value

    table["ra"].unit = degree
    table["dec"].unit = degree

    table["ra"][table["ra"] > 180*degree] -= 360*degree
    table["ra"][table["ra"] < -180*degree] += 360*degree

    table["luminosity_distance"] = cosmo.luminosity_distance(table["redshift"])

    complete_positions(table, to_complete={"cartesian", "spherical"})

    if only_in_grid:
        return cut_grid(table)
    return table


def read_SNe(filename, only_in_grid=True) -> QTable:
    """
    Read the SuperNovae data from a csv file

    Parameters
    ----------
    filename : str
        Path or name of the file
    only_in_grid : bool
        If True, only keep the SNe that are in the same box as the dark matter data

    Return
    ------
    table : astropy.QTable
        Table containing the SNe informations, in particular their spherical and cartesian coordinates
    """
    table = QTable.read(complete_path(filename), format="csv")

    table.rename_column("RA", "ra")
    table.rename_column("Dec", "dec")

    table["ra"] = list(map(convertra, table["ra"])) * degree
    table["ra"] = (
        table["ra"]
        + 2 * np.pi * (table["ra"] < -np.pi * rad) * rad
        - 2 * np.pi * (table["ra"] > np.pi * rad) * rad
    )

    table["dec"] = list(map(convertdec, table["dec"])) * degree
    table["luminosity_distance"] = cosmo.luminosity_distance(table["redshift"])

    complete_positions(table, to_complete={"cartesian", "spherical"})

    if only_in_grid:
        return cut_grid(table)
    return table


def read_bilby(
    run_name, dir=None, N=20000, only_in_grid=False, resample=False
) -> QTable:
    """
    Read a bilby results file and returns the posterior after
    adding the cartesian and spherical coordinates of the points

    Parameters
    ----------
    filename : str
        Path or name of the file
    only_in_grid : bool
        If True, only keep the points that are in the same box as the dark matter data

    Return
    ------
    table : astropy.QTable
        Table containing the bilby posterior
    """

    try:
        filename = complete_path(run_name + ".ecsv")
        table = QTable.read(filename)
        if only_in_grid:
            return cut_grid(table)
        return table
    except FileNotFoundError:
        try:
            print(
                "No ecsv file detected, looking for {}_result.json.....".format(
                    run_name
                )
            )
            filename = complete_path(run_name + "_result.json")
        except FileNotFoundError:
            print(
                "No json file detected, looking for {}_result.pkl......".format(
                    run_name
                )
            )
            filename = complete_path(run_name + "_result.pkl")

        result = read_in_result(filename=filename)

        table = QTable.from_pandas(result.posterior)
        table["ra"].unit = rad
        table["dec"].unit = rad
        table["luminosity_distance"].unit = Mpc

        table["ra"] = (
            table["ra"]
            + 2 * np.pi * (table["ra"] < -np.pi * rad) * rad
            - 2 * np.pi * (table["ra"] > np.pi * rad) * rad
        )
        table["dec"] = (
            table["dec"]
            + np.abs(table["dec"] + np.pi / 2 * rad) * (table["dec"] < -np.pi / 2 * rad)
            - np.abs(table["dec"] - np.pi / 2 * rad) * (table["dec"] > np.pi / 2 * rad)
        )

        complete_positions(table, to_complete={"cartesian", "spherical"})

        if only_in_grid:
            return cut_grid(table)
        if dir:
            table.write("outdir/{}/{}.ecsv".format(dir, run_name), overwrite=True)
        else:
            table.write("outdir/{}/{}.ecsv".format(run_name, run_name), overwrite=True)

        return table


def read_restrict(
    run_name,
    dir=None,
    only_in_grid=False,
    shift=-0.3,
    alpha=4.5,
) -> QTable:
    try:
        table = QTable.read(
            complete_path(run_name + "_" + str(shift) + "_restrict.ecsv")
        )
        return table
    except FileNotFoundError:
        print(
            "No ecsv file detected for threshold at {}, creating restriction from posterior {}.ecsv.....".format(
                shift, run_name
            )
        )
        posterior = read_bilby(run_name, only_in_grid=only_in_grid)
        points_coord = np.vstack([*get_posterior_axis(posterior, "cart")]).T

        _, _, den_r, tree = read_dark_matter("mcmc_10000.h5")
        in_dm_smooth = points_in_dm_smoothed(
            points_coord, tree, den_r, shift=shift, alpha=alpha
        )
        table = posterior[in_dm_smooth].copy()

        if only_in_grid:
            return cut_grid(table)
        if dir:
            table.write(
                "outdir/{}/{}_{}_restrict.ecsv".format(dir, run_name, str(shift)),
                overwrite=True,
            )
        else:
            table.write(
                "outdir/{}/{}_{}_restrict.ecsv".format(run_name, run_name, str(shift)),
                overwrite=True,
            )
        return table


def read_fits(event_name: str, only_in_grid=False) -> QTable:
    """
    Read a bilby results file and returns the posterior after
    adding the cartesian and spherical coordinates of the points

    Parameters
    ----------
    event_name : str
        Name of the event
    only_in_grid : bool
        If True, only keep the points that are in the same box as the dark matter data

    Return
    ------
    table : astropy.QTable
        Table containing the bilby posterior
    """

    try:
        table = QTable.read(complete_path(event_name + ".ecsv"))
        return table

    except FileNotFoundError:
        from src.priors import HealPixMapPriorDist

        print(
            "No ecsv file detected, reading from {}.fits............".format(event_name)
        )
        filename = complete_path(event_name + ".fits")

        priordist = HealPixMapPriorDist(filename, distance=True)

        print("Generating sample")
        priordist.sample(20000)

        table = QTable()
        table["ra"] = priordist.current_sample["ra"] * rad
        table["dec"] = priordist.current_sample["dec"] * rad
        table["luminosity_distance"] = priordist.current_sample["distance"] * Mpc

        table["ra"] = (
            table["ra"]
            + 2 * np.pi * (table["ra"] < 0 * rad) * rad
            - 2 * np.pi * (table["ra"] > 2 * np.pi * rad) * rad
        )
        table["dec"] = (
            table["dec"]
            + np.abs(table["dec"] + np.pi / 2 * rad) * (table["dec"] < -np.pi / 2 * rad)
            - np.abs(table["dec"] - np.pi / 2 * rad) * (table["dec"] > np.pi / 2 * rad)
        )

        complete_positions(table, to_complete={"cartesian", "spherical"})

        if only_in_grid:
            table = cut_grid(table)

        if not (os.path.exists("outdir/{}".format(event_name))):
            os.mkdir("outdir/{}".format(event_name))
        table.write("outdir/{}/{}.ecsv".format(event_name, event_name), overwrite=True)

        return table


def read_candidates(
    label: str,
    event_url: str = "https://www.wis-tns.org/ligo/event/LIGO_-_GW20190425_081805",
) -> QTable:
    """
    Read the potential candidates for a GW event from TNS

    Parameters
    ----------
    label : str
        Name of the event for local file management
    event_url : str
        Complete url of the GW event

    Return
    ------
    table : astropy.QTable
        Table containing candidates for the event

    """
    try:
        table = QTable.read(complete_path(f"{label}/candidates.ecsv"))
        return table
    except FileNotFoundError:
        print("No ecsv file found, getting candidates from TNS........")
        table = get_candidates(event_url)

        table["luminosity_distance"] = cosmo.luminosity_distance(table["redshift"])
        table = table[(table["luminosity_distance"] < 500)]

        complete_positions(table, to_complete={"cartesian", "spherical"})
        table.write(f"outdir/{label}/candidates.ecsv")
        return table
