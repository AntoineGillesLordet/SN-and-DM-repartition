"""
Transient Name Server website scrapping to get the candidates of a GW event
"""

import requests
import regex
from astropy.table import QTable
from astropy.units import degree
import time
import numpy as np


headers = {
    "User-agent": 'tns_marker{"tns_id":3120,"type": "user", "name":"antoine_gl"}'
}


def get_reset_time(response) -> int | None:
    for name in response.headers:
        value = response.headers.get(name)
        if name.endswith("-remaining") and value == "0":
            return int(response.headers.get(name.replace("remaining", "reset")))
    return None


def get_candidates(
    event_url,
    headers={
        "User-agent": 'tns_marker{"tns_id":3120,"type": "user", "name":"antoine_gl"}'
    },
) -> QTable:
    s = requests.Session()
    r = s.get(event_url, headers=headers)
    candidates_id = regex.findall(
        r'href="(/object/[0-9]+[a-z]*)', r.text.split(sep="before-objects")[0]
    )

    table = QTable(
        names=["id", "object_type", "ra", "dec", "redshift"],
        dtype=[str, str, float, float, float],
        units=[None, None, degree, degree, None],
    )

    reset_time = 1

    for id in candidates_id:
        candidate_name = id[8:]
        s1 = requests.Session()
        print(f"Getting data for {candidate_name}")
        while True:
            sleep_time = reset_time
            r1 = s1.get("https://www.wis-tns.org" + id, headers=headers)
            if r1.status_code == 429:
                print(f"Too many requests, waiting {sleep_time}s and retrying")
                if sleep_time:
                    time.sleep(sleep_time)
                else:
                    time.sleep(1)
            elif r1.status_code == 200:
                reset_time = get_reset_time(r1)
                ra, dec = regex.findall(
                    r'"alter-value">([0-9]+\.[0-9]+) ([+,-][0-9]+\.[0-9]+)', r1.text
                )[0]

                try:
                    redshift = float(
                        regex.findall(
                            r'Redshift<\/span><div class="value"><b>([0-9,.]+)', r1.text
                        )[0]
                    )
                except IndexError:
                    redshift = np.nan

                try:
                    object_type = regex.findall(
                        r'Type<\/span><div class="value"><b>([A-Z, a-z]+)', r1.text
                    )[0]
                except IndexError:
                    object_type = ""

                print(
                    (
                        candidate_name,
                        object_type,
                        float(ra) * degree,
                        float(dec) * degree,
                        redshift,
                    )
                )
                table.add_row(
                    [
                        candidate_name,
                        object_type,
                        float(ra) * degree,
                        float(dec) * degree,
                        redshift,
                    ]
                )
            else:
                break

    return table
