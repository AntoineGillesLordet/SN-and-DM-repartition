"""
Utils functions for data loading
"""

import glob
import os
from astropy.coordinates import SkyCoord
from astropy.units import Quantity, Unit, Mpc, hourangle, degree, arcmin, arcsec
from astropy.table import QTable

import numpy as np
from src.const import comov_to_ld, ld_to_comov, N, boxsize, cosmo


def complete_path(filename: str) -> str:
    """
    Find a file by its name and complete the path if necessary

    Parameters
    ----------
    filename : str
        Name of the file

    Return
    ------
    path : str
        Completed path
    """
    if os.path.exists(filename):
        return filename
    files = glob.glob("../**/" + filename, recursive=True)
    if files:
        if len(files) > 1:
            print("WARNING : several files found, used {}".format(files[0]))
        return files[0]
    else:
        raise FileNotFoundError("No files found")


def gridListPermutation(N: int = N, perm=(0, 1, 2)) -> np.ndarray:
    """
    Loading shenaningans to generate a 3d grid with potentially permutated axis.
    It also reshape an array in the right way to get the coordinates of the BORG data grid.

    Parameters
    ----------
    N : int
        Size of the grid
    perm : tuple
        Permutation of the axis to apply, default is the identity (0 1 2).

    Return
    ------
    ind : np.ndarray
        grid with permutated axis
    """

    if all(np.sort(perm) != np.array([0, 1, 2])):
        raise Exception("Must supply a permutation of 0,1,2")
    ind = np.zeros((N**3, 3), dtype="int")
    ind[:, perm[0]] = np.repeat(range(0, N), N**2)
    ind[:, perm[1]] = np.tile(np.repeat(range(0, N), N), N)
    ind[:, perm[2]] = np.tile(range(0, N), N**2)
    return ind


def cut_grid(table: QTable, unit: Unit = Mpc) -> None:
    return table[
        (table["x"] > -boxsize / 2 * unit)
        & (table["x"] < boxsize / 2 * unit)
        & (table["y"] > -boxsize / 2 * unit)
        & (table["y"] < boxsize / 2 * unit)
        & (table["z"] > -boxsize / 2 * unit)
        & (table["z"] < boxsize / 2 * unit)
    ]


def convertra(strhour: str) -> Quantity:
    """
    Converts a string angle "hh:mm:ss" (hours minutes seconds) to an astropy angle in degree.
    Used to convert the ra column of SN datafile.

    Parameters
    ----------
    strhour : str
        Angle as a string

    Return
    ------
    angle : astropy.units.Quantity
        Angle in degree
    """
    h, m, s = float(strhour[0:2]), float(strhour[3:5]), float(strhour[6:])
    return ((h + m / 60 + s / 3600) * hourangle).to(degree)


def convertdec(strhour: str) -> Quantity:
    """
    Converts a string angle "+dd:mm:ss" (sign degrees minutes seconds) to an astropy angle in degree.
    Used to convert the dec column of SN datafile.

    Parameters
    ----------
    strhour : str
        Angle as a string

    Return
    ------
    angle : astropy.units.Quantity
        Angle in degree
    """
    d, m, s = float(strhour[0:3]), float(strhour[4:6]), float(strhour[7:])
    return (d * degree + m * arcmin + s * arcsec).to(degree)


def complete_positions(
    table: QTable, to_complete: set = {"cartesian", "radec", "spherical"}
) -> None:
    """
    Complete a data table by adding the missing coordinates representations in new columns

    Parameters
    ----------
    table : QTable
        Table to fill
    to_complete : set
        Coordinates to add
    """

    radec_ok = np.all(
        [x in table.columns.keys() for x in ["ra", "dec", "luminosity_distance"]]
    )
    comov_cart_ok = np.all([x in table.columns.keys() for x in ["x", "y", "z"]])
    comov_spherical_ok = np.all(
        [x in table.columns.keys() for x in ["R", "Theta", "Phi"]]
    )

    if not (radec_ok) and not (comov_cart_ok) and not (comov_spherical_ok):
        raise ValueError(
            "The table must have at least positions in one of the following coordinates system :\n\tComoving Cartesian, Comoving Spherical, RA/DEC/luminosity distance"
        )

    if radec_ok:
        coords = SkyCoord(
            ra=table["ra"],
            dec=table["dec"],
            distance=ld_to_comov(table["luminosity_distance"]) * Mpc,
        )
    elif comov_cart_ok:
        coords = SkyCoord(
            x=table["x"], y=table["y"], z=table["z"], representation_type="cartesian"
        )
    elif comov_spherical_ok:
        coords = SkyCoord(ra=table["Phi"], dec=table["Theta"], distance=table["R"])

    for proj in to_complete:
        if proj == "cartesian" and not (comov_cart_ok):
            table["x"] = coords.icrs.cartesian.x
            table["y"] = coords.icrs.cartesian.y
            table["z"] = coords.icrs.cartesian.z

        if proj == "spherical" and not (comov_spherical_ok):
            table["R"] = coords.icrs.spherical.distance
            table["Theta"] = coords.icrs.spherical.lat
            table["Phi"] = coords.icrs.spherical.lon

        if proj == "radec" and not (radec_ok):
            if "luminosity_distance" not in table.columns.keys():
                table["luminosity_distance"] = (
                    comov_to_ld(coords.icrs.spherical.distance) * Mpc
                )
            table["ra"] = coords.icrs.spherical.lon
            table["dec"] = coords.icrs.spherical.lat
