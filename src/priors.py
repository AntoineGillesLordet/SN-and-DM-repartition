"""
Prior following a borg grid 
"""

from src.const import boxsize

import numpy as np
from numpy.typing import NDArray
from bilby.core.prior import BaseJointPriorDist, JointPrior, JointPriorDistError
from typing import Callable

from astropy.coordinates import SkyCoord
from astropy.units import Unit, rad
from astropy.cosmology import Cosmology, LambdaCDM
from scipy.interpolate import interp1d


class BORGPriorDist(BaseJointPriorDist):
    """
    Class defining prior according to given BORG grid

    Parameters
    ==========

    borg_file : file path to .fits file
        .fits file that contains the 2D or 3D Healpix Map
    bounds : dict or list (optional)
        dictionary or list with given prior bounds. defaults to normal bounds on ra, dev and 0, inf for distance
        if this is for a 3D map

    Returns
    =======

    PriorDist : `bilby.gw.prior.HealPixMapPriorDist`
        A JointPriorDist object to store the joint prior distribution according to passed healpix map
    """

    def __init__(
        self,
        borg_file: str,
        bounds: list[list[float]] | None = None,
        cosmo: Cosmology | None = None,
        filter: Callable[[NDArray], NDArray] | None = None,
    ) -> None:
        self.borg_file = borg_file
        names = ["ra", "dec", "luminosity_distance"]
        if cosmo is None:
            H0, Om0 = 67.66, 0.3111
            Ode0 = 1.0 - Om0
            self.cosmo = LambdaCDM(H0, Om0, Ode0)

        self.comov_to_ld = interp1d(
            self.cosmo.comoving_distance(np.linspace(0, 0.2, 100000)),
            self.cosmo.luminosity_distance(np.linspace(0, 0.2, 100000)),
            kind="quadratic",
            copy=True,
            bounds_error=True,
        )
        self.ld_to_comov = interp1d(
            self.cosmo.luminosity_distance(np.linspace(0, 0.2, 100000)),
            self.cosmo.comoving_distance(np.linspace(0, 0.2, 100000)),
            kind="quadratic",
            copy=True,
            bounds_error=True,
        )

        from src.load import read_dark_matter

        _, self.positions, self.density, self.tree = read_dark_matter(borg_file)

        if filter:
            self.prob = filter(self.density)
        else:
            self.prob = self._sigmoid(self.density)

        self.prob[
            (np.linalg.norm(self.positions, ord=2, axis=1) > boxsize / 2)
            | (np.linalg.norm(self.positions, ord=2, axis=1) < 50)
        ] = 0
        self.prob = self._check_norm(self.prob)

        self.npix = len(self.prob)
        self.N = np.cbrt(self.npix)
        self.pixel_length = boxsize / self.N
        self.pixel_volume = self.pixel_length**3

        if bounds is None:
            bounds = [
                [0, 2 * np.pi],
                [-np.pi / 2.0, np.pi / 2.0],
                [
                    float(self.comov_to_ld((50 - np.sqrt(3) * self.pixel_length) / 2)),
                    float(
                        self.comov_to_ld((boxsize + np.sqrt(3) * self.pixel_length) / 2)
                    ),
                ],
            ]

        elif isinstance(bounds, dict):
            bs = [[] for _ in bounds.keys()]
            for i, key in enumerate(bounds.keys()):
                bs[i] = (bounds[key][0], bounds[key][1])
            bounds = bs

        super(BORGPriorDist, self).__init__(names=names, bounds=bounds)
        self.distname = "BORG"
        self.pix_xx = np.arange(self.npix)
        self._all_interped = interp1d(
            x=self.pix_xx, y=self.prob, bounds_error=False, fill_value=0
        )

        self._build_attributes()

    def _build_attributes(self) -> None:
        """
        Method that builds the inverse cdf of the P(pixel) distribution for rescaling
        """
        from scipy.integrate import cumtrapz

        yy = self._all_interped(self.pix_xx)
        yy /= np.trapz(yy, self.pix_xx)
        YY = cumtrapz(yy, self.pix_xx, initial=0)
        YY[-1] = 1
        self.inverse_cdf = interp1d(x=YY, y=self.pix_xx, bounds_error=True)

    def _rescale(self, samp: NDArray) -> NDArray:
        """
        Overwrites the _rescale method of BaseJoint Prior to rescale a single value from the unitcube onto
        three values (ra, dec, ld)

        Parameters
        ==========
        samp : float, int
            must take in single value for pixel on unitcube to rescale onto ra, dec, distance for the map Prior

        Returns
        =======
        rescaled_sample : array_like
            sample to rescale onto the prior
        """
        samp = samp[:, 0]
        pix_rescale = self.inverse_cdf(samp)
        sample = np.empty((len(pix_rescale), 3))

        for i, val in enumerate(pix_rescale):
            sample[i, :] = self.draw_from_pixel(int(round(val)))[::-1]

        return sample.reshape((-1, self.num_vars))

    @staticmethod
    def _sigmoid(x: NDArray, alpha: float = 4.5, center: float = -0.3) -> NDArray:
        """
        static method to

        Parameters
        ==========
        array : float or array_like
            input

        Returns
        =======
        values : float array_like
            returns value of sigmoid at the input points
        """
        return 1 / (1 + np.exp(-alpha * (x - center)))

    @staticmethod
    def _check_norm(array: NDArray) -> NDArray:
        """
        static method to check if array is properlly normalized and if not to normalize it.

        Parameters
        ==========
        array : array_like
            input array we want to renormalize if not already normalized

        Returns
        =======
        normed_array : array_like
            returns input array normalized
        """
        norm = np.linalg.norm(array, ord=1)
        if norm == 0:
            norm = np.finfo(array.dtype).eps
        return array / norm

    def _sample(self, size: int) -> NDArray:
        """
        Overwrites the _sample method of BaseJoint Prior. Picks a pixel value according to their probabilities, then
        uniformly samples ra, and decs that are contained in chosen pixel. If the PriorDist includes distance it then
        updates the distance distributions and will sample according to the conditional distance distribution along a
        given line of sight

        Parameters
        ==========
        size : int
            number of samples we want to draw

        Returns
        =======
        sample : array_like
            sample of ra, and dec (and distance if 3D=True)
        """
        pixel_choices = np.arange(self.npix)
        sample_pix = np.random.choice(
            pixel_choices, size=size, p=self.prob, replace=True
        )
        sample = np.empty((size, self.num_vars))
        for samp in range(size):
            sample[samp, :] = self.draw_from_pixel(sample_pix[samp])
        return sample.reshape((-1, self.num_vars))

    def draw_from_pixel(self, pix: int) -> NDArray:
        """
        Recursive function to uniformly draw ra, and dec values that are located in the given pixel

        Parameters
        ==========
        pix : int
            pixel index for given pixel we want to get ra, and dec from

        Returns
        =======
        ra_dec_ld : ndarray
            this returns an array of ra, dec and ld sampled uniformly that are in the pixel given
        """
        x, y, z = self.positions[pix]
        draw_x = np.random.uniform(x - self.pixel_length / 2, x + self.pixel_length / 2)
        draw_y = np.random.uniform(y - self.pixel_length / 2, y + self.pixel_length / 2)
        draw_z = np.random.uniform(z - self.pixel_length / 2, z + self.pixel_length / 2)

        r = np.linalg.norm([draw_x, draw_y, draw_z], ord=2)

        draw_ra = np.arctan2(draw_y, draw_x)
        draw_dec = np.arcsin(draw_z / r)

        draw_ld = self.comov_to_ld(r)

        return np.array([draw_ra, draw_dec, draw_ld])

    def _ln_prob(self, samp: NDArray, lnprob: NDArray, outbounds: NDArray) -> NDArray:
        """
        Overwrites the _lnprob method of BaseJoint Prior

        Parameters
        ==========
        samp : array_like
            samples of ra, dec, ld to evaluate the lnprob at
        lnprob : array_like
            array of correct length we want to populate with lnprob values
        outbounds : boolean array
            boolean array that flags samples that are out of the given bounds

        Returns
        =======
        lnprob : array_like
            lnprob values at each sample
        """
        for i in range(samp.shape[0]):
            if not outbounds[i]:
                ra, dec, ld = samp[i]
                coord = SkyCoord(
                    ra=ra * rad, dec=dec * rad, distance=self.ld_to_comov(ld)
                )
                _, pixel = self.tree.query(
                    [
                        coord.icrs.cartesian.x + boxsize / 2,
                        coord.icrs.cartesian.y + boxsize / 2,
                        coord.icrs.cartesian.z + boxsize / 2,
                    ],
                    k=1,
                )
                lnprob[i] = np.log(self.prob[pixel] / self.pixel_volume)
        lnprob[outbounds] = -np.inf
        return lnprob

    def __eq__(self, other) -> bool:
        skip_keys = ["_all_interped", "inverse_cdf"]
        if self.__class__ != other.__class__:
            return False
        if sorted(self.__dict__.keys()) != sorted(other.__dict__.keys()):
            return False
        for key in self.__dict__:
            if key in skip_keys:
                continue
            if key == "borg_file":
                if self.__dict__[key] != other.__dict__[key]:
                    return False
            elif isinstance(self.__dict__[key], (np.ndarray, list)):
                thisarr = np.asarray(self.__dict__[key])
                otherarr = np.asarray(other.__dict__[key])
                if thisarr.dtype == float and otherarr.dtype == float:
                    fin1 = np.isfinite(np.asarray(self.__dict__[key]))
                    fin2 = np.isfinite(np.asarray(other.__dict__[key]))
                    if not np.array_equal(fin1, fin2):
                        return False
                    if not np.allclose(thisarr[fin1], otherarr[fin2], atol=1e-15):
                        return False
                else:
                    if not np.array_equal(thisarr, otherarr):
                        return False
            else:
                if not self.__dict__[key] == other.__dict__[key]:
                    return False
        return True


class BORGPrior(JointPrior):
    """
    A prior distribution that follows a user-provided HealPix map for one
    parameter.

    See :code:`bilby.gw.prior.HealPixMapPriorDist` for more details of how to
    instantiate the prior.
    """

    def __init__(
        self,
        dist: BORGPriorDist,
        name: str | None = None,
        latex_label: str | None = None,
        unit: Unit = None,
    ) -> None:
        """

        Parameters
        ----------
        dist: bilby.gw.prior.HealPixMapPriorDist
            The base joint probability.
        name: str
            The name of the parameter, it should be contained in the map.
            One of ["ra", "dec", "luminosity_distance"].
        latex_label: str
            Latex label used for plotting, will be read from default values if
            not provided.
        unit: str
            The unit of the parameter.
        """
        if not isinstance(dist, BORGPriorDist):
            raise JointPriorDistError("dist object must be instance of BORGPriorDist")
        super(BORGPrior, self).__init__(
            dist=dist, name=name, latex_label=latex_label, unit=unit
        )
