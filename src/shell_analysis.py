"""
Shell extraction and map generation for various data
"""

from src.const import boxsize, nside, fwhm
from src.utils import get_levels_from_quantiles
import healpy
import numpy as np
from numpy.typing import NDArray

from astropy.table import QTable
from astropy import units as u

from scipy.spatial._ckdtree import cKDTree

from typing import overload, Tuple


@overload
def extract_shell(*, df: QTable, radius: float, dr: float, **kwargs) -> QTable: ...


@overload
def extract_shell(
    *,
    centroids: NDArray,
    positions: NDArray,
    den_r: NDArray,
    tree: cKDTree,
    radius: float,
    dr: float,
    **kwargs,
) -> Tuple: ...


def extract_shell(
    *args,
    radius: float,
    dr: float = 1,
    **kwargs,
) -> QTable | tuple:
    """
    Extract the points in a shell at some radius for a SN or galaxies dataframe, or a BORG grid.
    Note that radius is a keyword-only argument.

    Parameters
    ----------
    df : DataFrame
        Datas to consider. Either a DataFrame for SN and galaxies,
        or centroids for dark matter
    centroids : (int list) list
        Coordinates of the centroids in the box
    positions : (float list) list
        Physical coordinates at each point in the box
    den_r : float list
        Densities at each point in the box
    tree : scipy.spatial.CKDTree
        Tree of the points
    radius : float
        Inner shell radius
    dr : float
        Shell thickness

    Return
    ------
    extracted : DataFrame or Tuple
        DataFrame of the SNe or galaxies in the shell, or centroids, positions and den_r of the points in the shell
    """

    if args and isinstance(args[0], QTable):
        return args[0][(args[0]["R"] > radius) & (args[0]["R"] < radius + dr)].copy()
    elif args and len(args) == 4:
        centroids, positions, den_r, tree = args[:4]
        center = kwargs.pop("center", [boxsize / 2] * 3)
        near = set(tree.query_ball_point(center, radius))
        far = set(tree.query_ball_point(center, radius + dr))
        shell = list(far - near)
        return centroids[shell], positions[shell], den_r[shell]
    else:
        raise TypeError("Incorrect input")


def generate_map(
    data: QTable | NDArray,
    values: NDArray | None = None,
    representation_type="radec",
    nside: int = nside,
    average: bool = False,
    smooth: bool = True,
    fwhm: float = 0,
    positive: bool = False,
) -> NDArray | tuple:
    """
    Constructs the projected map of a collection of points on a spherical surface

    Parameters
    ----------
    data : QTable or numpy.ndarray
        Coordinates of the points to project on the map, either [ra, dec] in rad or [x,y,z] cartesian coordinates.
        Also works with astropy QTable, if the provided table has columns `'ra','dec'` or `'x','y','z'`
    values : float numpy.ndarray
        Values of a quantity to use at those points
    representation_type : float numpy.ndarray
        Representation used, either radec or xyz cartesian.
    nside : int, optional
        nside parameter for healpy
    average : bool, default True
        Average along the line of sight of each healpy pixels. If ``False``, sum the projected points value.
    smooth : bool, default True
        If `True`, apply the healpy gaussian blur to the map
    fwhm : float, default 0
        The full width half max parameter of the Gaussian (in radians) for the smoothing
    positive : bool, default False
        If `True`, search for the smallest increment making the map entirely positive. Useful for log plotting.

    Return
    ------
    map : float list
        Obtained healpy map
    inc : int, optionnal
        If positive was `True`, increment to add to map for log plotting
    """
    if representation_type == "radec":
        if isinstance(data, QTable):
            ra, dec = data["ra"].to_value(u.rad), data["dec"].to_value(u.rad)
        elif isinstance(data, np.ndarray):
            if len(data) != 2:
                raise ValueError(
                    f"`generate_map` is in radec mode, but the provided data has dimension {data.shape} instead of (2,n)."
                )
            try:
                ra, dec = data[0].to_value(u.rad), data[1].to_value(u.rad)
            except AttributeError:
                ra, dec = data[0], data[1]
        else:
            raise TypeError(f"Unsupported type for data : {type(data)}")
        ipix = healpy.ang2pix(nside, np.pi / 2 - dec, ra)

    elif representation_type == "cartesian":
        if isinstance(data, QTable):
            x, y, z = data["x"], data["y"], data["z"]
        elif isinstance(data, np.ndarray):
            if len(data) != 3:
                raise ValueError(
                    f"`generate_map` is in cartesian mode, but the provided data has dimension {data.shape} instead of (3,n)."
                )
            x, y, z = data[0], data[1], data[2]
        else:
            raise TypeError(f"Unsupported type for data : {type(data)}")
        ipix = healpy.vec2pix(nside, x, y, z)

    else:
        raise ValueError(
            f"Representation type not recognized, musst be one of : 'radec', 'cartesian'."
        )

    map_values = np.zeros(healpy.nside2npix(nside), dtype=float)

    nb_points = np.zeros_like(map_values)

    for i in range(len(ipix)):
        if values is not None:
            map_values[ipix[i]] += values[i]
        nb_points[ipix[i]] += 1

    if average:
        map_values = np.nan_to_num(map_values / nb_points)

    if smooth:
        if values is not None:
            map_values = healpy.sphtfunc.smoothing(map_values, fwhm=fwhm)
        else:
            map_values = healpy.sphtfunc.smoothing(nb_points, fwhm=fwhm)

    inc = 0
    if positive:
        inc = -int(np.floor(map_values.min()))
        return map_values, inc

    return map_values, None


def get_contour_levels(
    posterior: QTable,
    quantiles: NDArray | None = None,
    nside: int = 256,
    fwhm: float = 0.09,
) -> tuple[list, NDArray]:
    """
    Get the levels at which to apply the cut on the map to get the corresponding quantile contour.
    Default are gaussian 1, 2 and 3 sigma contours.
    """
    if quantiles is None:
        quantiles = 1 - np.exp(-np.array([4.5, 2, 0.5]))

    map, _ = generate_map(
        posterior, representation_type="radec", nside=nside, fwhm=fwhm
    )

    V = get_levels_from_quantiles(map, quantiles)

    return map, V
