"""
Utility functions
"""

import numpy as np
from numpy.typing import NDArray
from astropy import units as u
from astropy.units import Unit
from astropy.table import QTable


def get_posterior_axis(
    posterior: QTable,
    proj: str,
    units: dict[str, Unit] = {"angle": u.rad, "length": u.Mpc},
) -> tuple:
    """
    Returns the posterior axis in the chosen units with the chosen projection.

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    proj : ``'cart'`` or ``'radec'``
        Projection to use
    units : dict, optional
        Units to use. Default is angles in radians and distances in Mpc.
    """

    if proj == "radec":
        posterior_c1 = posterior["ra"].to(units["angle"])
        posterior_c2 = posterior["dec"].to(units["angle"])
        posterior_c3 = posterior["luminosity_distance"].to(units["length"])

    elif proj == "cart":
        posterior_c1 = posterior["x"].to(units["length"])
        posterior_c2 = posterior["y"].to(units["length"])
        posterior_c3 = posterior["z"].to(units["length"])

    else:
        raise ValueError(
            "Projection not recognized, must be one of : `radec` or `cart`"
        )

    return posterior_c1, posterior_c2, posterior_c3


def get_levels_from_quantiles(values: NDArray, quantiles: NDArray) -> NDArray:
    values_ = values.copy()
    values_ = values_.flatten()
    inds = np.argsort(values_)[::-1]
    values_ = values_[inds]
    cs = np.cumsum(values_)

    cs /= cs[-1]
    V = np.empty(len(quantiles))
    for i, v0 in enumerate(quantiles):
        try:
            V[i] = values_[cs <= v0][-1]
        except IndexError:
            V[i] = values_[0]
    V.sort()
    m = np.diff(V) == 0
    while np.any(m):
        V[np.where(m)[0][0]] *= 1.0 - 1e-4
        m = np.diff(V) == 0
    V.sort()
    V = V[::-1]
    return V
