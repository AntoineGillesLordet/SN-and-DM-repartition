from typing import Any
from trimesh import Trimesh
from numpy.typing import NDArray
from astropy.table import QTable
from astropy.units import Unit

import numpy as np
from skimage import measure
from scipy.ndimage import gaussian_filter
from src.const import boxsize, N
from src.utils import get_levels_from_quantiles
from astropy import units as u


def mesh_hull(
    dist: NDArray,
    spacing: NDArray,
    origin: NDArray = np.array([0, 0, 0]),
    level: Any = None,
    expand: bool = False,
) -> Trimesh:
    """
    Get the hull of a 3D distribution at some level as a triangular mesh

    Parameters
    ----------
    dist : np.ndarray
        Distribution on a grid to consider
    spacing : np.ndarray
        Discretization step of the grid for the three axis
    origin : np.ndarray, optional
        Origin shift for all the points
    level : float, optional
        Level to cut the distribution values at. Default is 50% of the maximum value.
    expand : bool, optional
        If True, add a -infinity padding around the distribution.
        Useful to ensure a closed mesh on the box boundaries.

    Return
    ------
    trim : trimesh.Trimesh
        The extracted mesh
    """

    if level is None:
        level = 0.5 * np.max(dist)

    if expand:
        n, m, p = dist.shape
        expanded_dist = -np.inf * np.ones((n + 2, m + 2, p + 2))
        expanded_dist[1:-1, 1:-1, 1:-1] = dist
        verts, faces, normals, _ = measure.marching_cubes(
            expanded_dist, level, spacing=spacing
        )
        verts = verts - spacing
    else:
        verts, faces, normals, _ = measure.marching_cubes(dist, level, spacing=spacing)

    verts = verts + origin

    trim = Trimesh(vertices=verts, faces=faces, faces_normals=normals)
    trim.fix_normals()

    return trim


def successive_shells(
    posterior: QTable,
    quantiles: Any = None,
    bins: int | NDArray = 50,
    unit: Unit = u.Mpc,
    sigma: float = 0.9,
) -> list[Trimesh] | Trimesh:
    """
    Get shells of the posterior at some levels. Default is 1, 2 and 3 sigma volumes of the posterior.
    Also work for a single shell.

    Parameters
    ----------
    posterior : astropy.QTable
        Posterior points table
    quantiles : float or array like
        Quantiles of the density at which the shells are taken
    proj : str, optional
        Projection to use, `radec` or `cart`. Default is cartesian
    bins : int or np.ndarray
        Bins to use for histogramming
    units : dict, optional
        Units to use for the histogramming. Default is Mpc for distances.

    Return
    ------
    meshes : trimesh.Trimesh or trimesh.Trimesh list
        Posterior shells at each level
    """
    edges: NDArray | list
    H, edges = np.histogramdd(
        (
            posterior["x"].to(unit),
            posterior["y"].to(unit),
            posterior["z"].to(unit),
        ),
        bins=bins,
    )

    H_blur = gaussian_filter(H, sigma=sigma)

    edges = np.array(edges)
    spacing = np.mean((edges[:, 1:] - edges[:, :-1]), axis=1)
    origin = (edges.T[1] + edges.T[0]) / 2

    if quantiles is None:
        quantiles = 1 - np.exp(-np.array([0.5, 2, 4.5]))

    levels = get_levels_from_quantiles(H_blur, quantiles)

    try:
        return [
            mesh_hull(
                H_blur,
                spacing,
                level=level,
                origin=origin,
                expand=True,
            )
            for level in levels
        ]
    except TypeError:
        return mesh_hull(H_blur, spacing, level=levels, origin=origin, expand=True)


def dm_in_box(
    bounds: NDArray, positions: NDArray, den_r: NDArray, range_pos: NDArray
) -> tuple:
    """
    Extract the region of the BORG data grid in a box defined by its two corners.

    Parameters
    ----------
    bounds : np.array (2,3) float
        Bounds of the box to extract
    positions : (float list) list
        Physical coordinates of the dark matter points
    den_r : float list
        Dark matter overdensity at each point
    range_pos : float list
        Distinct possible values taken along each axis

    Return
    ------
    sub_pos : (float list) list
        Physical coordinates of the dark matter points that are near the posterior points
    sub_den : float list
        Dark matter overdensity at each of those points
    """

    where = (range_pos.T @ np.ones((1, 3)) > bounds[0]) & (
        range_pos.T @ np.ones((1, 3)) < bounds[1]
    )

    idmins, idmaxs = where.T.argmax(axis=1), (
        where.T.shape[1] - where.T[:, ::-1].argmax(axis=1)
    )

    sub_pos = positions.reshape(N, N, N, 3)[
        idmins[0] : idmaxs[0], idmins[1] : idmaxs[1], idmins[2] : idmaxs[2]
    ].reshape(-1, 3)
    sub_den = den_r.reshape(N, N, N)[
        idmins[0] : idmaxs[0], idmins[1] : idmaxs[1], idmins[2] : idmaxs[2]
    ].reshape(-1)

    return sub_pos, sub_den


def dm_hull(positions: NDArray, den_r: NDArray, level: Any = 0.0) -> Trimesh:
    """
    Extracts the mesh of the dark matter hull above the levels

    Parameters
    ----------
    positions : (float list) list
        Physical coordinates of the dark matter points
    den_r : float list
        Dark matter overdensity at each point
    level : float, optional
        Level at which to make the cut

    Return
    ------
    mesh : trimesh.Trimesh
        The mesh of the extracted volume
    """
    shape = [len(np.unique(positions[:, i])) for i in range(len(positions[0]))]
    spacing = boxsize / N * np.ones(3)
    return mesh_hull(
        den_r.reshape(*shape), spacing, origin=positions[0], level=level, expand=True
    )
